/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _FrameManager_h_
#define _FrameManager_h_ 1

#include "runtime/Debug.h"

template<size_t PL, char Type> class FrameMap : public FrameMap<PL+1,Type> {
  KernelMCSLock lock;
  static const size_t fsize = pagesize<PL>();
  static const size_t BlockSize = bitsize<mword>(); // 64 vs. 512 (ptentries)
  HierarchicalBitmap<framebits-pagesizebits<PL>(),BlockSize> frames;

  bool blockfull(size_t idx) {
    idx = align_down(idx, ptentries);
    for (size_t i = idx; i < idx + ptentries; i += BlockSize) {
      if (!frames.blockfull(i)) return false;
    }
    return true;
  }

  bool blockempty(size_t idx) {
    idx = align_down(idx, ptentries);
    for (size_t i = idx; i < idx + ptentries; i += BlockSize) {
      if (!frames.blockempty(i)) return false;
    }
    return true;
  }

  void blockset(size_t idx) {
    idx = align_down(idx, ptentries);
    for (size_t i = idx; i < idx + ptentries; i += BlockSize) {
      frames.blockset(i);
    }
  }

  void blockclr(size_t idx) {
    idx = align_down(idx, ptentries);
    for (size_t i = idx; i < idx + ptentries; i += BlockSize) {
      frames.blockclr(i);
    }
  }

public:
  inline void print(std::ostream& os, paddr base, size_t range);

  bool release1(size_t idx) {
    ScopedLock<KernelMCSLock> sl(lock);
    frames.set(idx);
    if (blockfull(idx)) { // full bitmap -> clear and mark larger frame
      if (FrameMap<PL+1,Type>::release1(idx / ptentries)) blockclr(idx);
    }
    return true;
  }

  void releaseN(size_t idx, size_t cnt) {
    while (cnt > 0) {
      if ( aligned(idx, ptentries) && cnt >= ptentries
        && FrameMap<PL+1,Type>::release1(idx / ptentries) ) {
        idx += ptentries;
        cnt -= ptentries;
      } else {
        release1(idx);
        idx += 1;
        cnt -= 1;
      }
    }
  }

  size_t alloc1() {
    ScopedLock<KernelMCSLock> sl(lock);
    size_t idx = frames.find();
    if (idx == limit<size_t>()) {
      idx = FrameMap<PL+1,Type>::alloc1();
      if (idx == limit<size_t>()) return limit<size_t>();
      idx *= ptentries;
      blockset(idx);
    }
    frames.clr(idx);
    return idx;
  }

  size_t allocN(size_t cnt, size_t lim) {
    ScopedLock<KernelMCSLock> sl(lock);
    size_t idx = 0;
    for (;;) {
      size_t c = frames.findrange(idx, lim);
      if (c == 0) {
        idx = FrameMap<PL+1,Type>::alloc1();
        if (idx == limit<size_t>()) return limit<size_t>();
        idx *= ptentries;
        blockset(idx);
        idx = 0; // restart loop
      }
      if (c >= cnt) {
        for (size_t i = 0; i < cnt; i += 1) frames.clr(idx + i);
        return idx;
      }
      idx += c;
    }
  }

  size_t memsize(size_t range) {
    return frames.memsize(divup(range, fsize))
         + FrameMap<PL+1,Type>::memsize(range);
  }

  void init(size_t range, bufptr_t p) {
    frames.init(divup(range, fsize), p);
    p += frames.memsize(divup(range, fsize));
    FrameMap<PL+1,Type>::init(range, p);
  }
};

template<char Type> class FrameMap<kernelpl+1,Type> {
public:
  bool release1(size_t) { return false; }
  void print(std::ostream&, paddr, size_t) {}
  size_t alloc1() { return limit<size_t>(); }
  size_t memsize(size_t) { return 0; }
  void init(size_t, bufptr_t) {}
};

void FrameZero(paddr pma, size_t size, _friend<FrameManager> ff);

class FrameManager {
  friend std::ostream& operator<<(std::ostream&, const FrameManager&);
  FrameMap<smallpl,'C'> cleanFrames;
  FrameMap<smallpl,'D'> dirtyFrames;
  paddr baseAddress;
  size_t memRange;

public:
  size_t preinit( paddr base, paddr top ) {
    baseAddress = base;
    memRange = top - base;
    return cleanFrames.memsize(memRange) + dirtyFrames.memsize(memRange);
  }

  void init( bufptr_t p ) {
    cleanFrames.init(memRange, p);
    p += cleanFrames.memsize(memRange);
    dirtyFrames.init(memRange, p);
  }

  void release( paddr addr, size_t size ) {
    RASSERT(aligned(addr, smallps), FmtHex(addr));
    RASSERT(aligned(size, smallps), FmtHex(size));
    DBG::outl(DBG::Frame, "FM/release: ", FmtHex(addr), '/', FmtHex(size));
    dirtyFrames.releaseN((addr - baseAddress) / smallps, size / smallps);
  }

  template<size_t N>
  inline paddr allocFrame() {
    size_t idx = cleanFrames.FrameMap<N,'C'>::alloc1();
    if (idx == limit<size_t>()) {                          // try cleaning a frame
      LocalProcessor::lock();
      if (!zeroMemory<N>()) while (zeroMemory<smallpl>()); // Hail Mary
      LocalProcessor::unlock();
      idx = cleanFrames.FrameMap<N,'C'>::alloc1();
      if (idx == limit<size_t>()) {
        DBG::outl(DBG::Basic, "OUT OF MEMORY");
        RebootDirect();
      }
    }
    paddr addr = baseAddress + idx * pagesize<N>();
    DBG::outl(DBG::Frame, "FM/alloc: ", FmtHex(addr), '/', FmtHex(pagesize<N>()));
    return addr;
  }

  paddr allocRegion(size_t& size, paddr align, paddr limitAddress) {
    RASSERT(align <= smallps, FmtHex(align));
    size = align_up(size, smallps);
    RASSERT(size <= pagesize<smallpl+1>(), FmtHex(size));
    size_t cnt = size / smallps;
    size_t lim = std::min((limitAddress - baseAddress), memRange) / smallps - cnt;
    size_t idx = cleanFrames.allocN(cnt, lim);
    if (idx == limit<size_t>()) {
      DBG::outl(DBG::Basic, "OUT OF CONTIG MEMORY");
      Reboot();
    }
    paddr addr = baseAddress + idx * smallps;
    DBG::outl(DBG::Frame, "FM/alloc: ", FmtHex(addr), '/', FmtHex(size));
    return addr;
  }

  template<size_t N>
  bool zeroMemory() {
    size_t idx = dirtyFrames.FrameMap<N,'D'>::alloc1();
    if (idx == limit<size_t>()) return false;
    paddr pma = baseAddress + idx * pagesize<N>();
    FrameZero(pma, pagesize<N>(), _friend<FrameManager>());
    MemoryFence();               // ensure that non-zeroed memory is not leaking
    cleanFrames.FrameMap<N,'C'>::release1(idx);
    return true;
  }
};

#endif /* _FrameManager_h_ */
