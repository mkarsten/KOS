/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _ManagedArray_h_
#define _ManagedArray_h_ 1

#include "runtime/Basics.h"

#include <vector>

// ManagedArray automatically keeps track of which fields are in use and
// which are unused.  Unused fields are stored in a free-stack, so can be
// found at small constant cost.  User needs to provide a FreeMask bit
// pattern that can be applied to the first word in element data structure
// to determine whether an array slot is in use or not.  I.e., that bit must
// never occur set in a valid instance.  Also, this bit must never mask a
// significant bit of a valid pointer for the target architecture.
template<typename T, mword FreeMask, template<typename> class Alloc>
class ManagedArray {

  union Element {
    T data;
    mword test;
    Element* next;
  }; // gcc does not like __packed here - why?

  static_assert(sizeof(T) >= sizeof(mword), "type size violation");
  static_assert(sizeof(T) >= sizeof(Element*), "type size violation");

  Alloc<Element> allocator;
  Element* data;
  Element* freestack;
  size_t index;
  size_t capacity;
  size_t freecount;

protected:
  void internalSet(size_t idx, const T& elem) {
    RASSERT(clear(idx), idx);
    data[idx].data = elem;
  }

  void internalRemove(size_t idx) {
    RASSERT(valid(idx), idx);
    data[idx].next = freestack;
    freestack = &data[idx];
    freestack->test |= FreeMask;
    freecount += 1;
  }

public:
  ManagedArray(size_t n = 0) : data(allocator.allocate(n)), freestack(nullptr),
    index(0), capacity(n), freecount(0) {}
  ~ManagedArray() { allocator.deallocate(data, capacity); }

  bool   valid(size_t idx) const { return idx < index && !(data[idx].test & FreeMask); }
  bool   clear(size_t idx) const { return idx < index &&  (data[idx].test & FreeMask); }
  size_t size()            const { return index - freecount; }
  size_t currentIndex()    const { return index; }
  size_t currentCapacity() const { return capacity; }
  bool   empty()           const { return size() == 0; }

  size_t reserveIndex() {
    if (freestack) {
      size_t idx = freestack - data;
      RASSERT(clear(idx), idx);
      freestack = (Element*)(uintptr_t(freestack->next) & ~FreeMask);
      freecount -= 1;
      return idx;
    } else if (capacity > index) {
      size_t idx = index;
      data[idx].test |= FreeMask;
      index += 1;
      return idx;
    } else {
      size_t newCapacity = capacity ? capacity * 2 : 1;
      Element* newData = allocator.allocate(newCapacity);
      if (!newData) return limit<size_t>();
      for (size_t i = 0; i < capacity; i += 1) newData[i] = data[i];
      allocator.deallocate(data, capacity);
      data = newData;
      capacity = newCapacity;
      return reserveIndex();
    }
  }

  void set(size_t idx, const T& elem) {
    internalSet(idx, elem);
  }

  size_t put(const T& elem) {
    size_t idx = reserveIndex();
    internalSet(idx, elem);
    return idx;
  }

  void remove(size_t idx) {
    internalRemove(idx);
  }

  T& access(size_t idx) const {
    RASSERT(valid(idx), idx);
    return data[idx].data;
  }

  bool retrieve(size_t idx, T& elem) {
    if (!valid(idx)) return false;
    elem = data[idx].data;
    internalRemove(idx);
    return true;
  }

};

#endif /* _ManagedArray_h_ */
