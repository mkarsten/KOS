/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RuntimeDebug_h_
#define _RuntimeDebug_h_ 1

#include "kernel/Output.h"

enum DBG::Level : size_t {
  Acpi = 0,
  AddressSpaceDbg,
  Basic,
  Blocking,
  Boot,
  CDI,
  Devices,
  Error,
  Frame,
  File,
  GDBDebug,
  GDBEnable,
  Idle,
  KernMem,
  Libc,
  Lwip,
  MemAcpi,
  Paging,
  PCI,
  Perf,
  ProcessDbg,
  Scheduling,
  Syscalls,
  Tests,
  Threads,
  VM,
  Warning,
  MaxLevel
};

template<typename... Args>
inline void DBG::out1(DBG::Level c, const Args&... a) {
  if (c && !test(c)) return;
  StdDbg.printl<false>(a...);
#if TESTING_DEBUG_STDOUT
  if (c) StdOut.printl<false>(a...);
#endif
}

template<typename... Args>
inline void DBG::outs(DBG::Level c, const Args&... a) {
  if (c && !test(c)) return;
  StdDbg.printl<true>(a...);
#if TESTING_DEBUG_STDOUT
  if (c) StdOut.printl<true>(a...);
#endif
}

template<typename... Args>
inline void DBG::outl(DBG::Level c, const Args&... a) {
  if (c && !test(c)) return;
  StdDbg.printl<true>(a..., kendl);
#if TESTING_DEBUG_STDOUT
  if (c) StdOut.printl<true>(a..., kendl);
#endif
}

inline void DBG::outl(DBG::Level c) {
  if (c && !test(c)) return;
  StdDbg.printl<false>(kendl);
#if TESTING_DEBUG_STDOUT
  if (c) StdOut.printl<false>(kendl);
#endif
}

// implemented in KernelDebug.cc
extern void DebugInit(char* dstring, bool print);
extern void ExternDebugPrintf(DBG::Level c, const char* fmt, va_list args);

#endif /* _RuntimeDebug_h_ */
