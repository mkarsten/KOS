/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _KernelProcessor_h_
#define _KernelProcessor_h_ 1

#include "runtime/BaseProcessor.h"
#include "runtime-glue/RuntimeContext.h"
#include "extern/mtrand/mtrand.h"

class Scheduler;

struct AddressSpaceMarker : public DoubleLink<AddressSpaceMarker> {
  volatile sword enterEpoch;
};

class KernelProcessor : public BaseProcessor, public HardwareProcessor {
  friend class KernelAddressSpace; // kernASM
  friend class AddressSpace;       // userASM
  AddressSpaceMarker kernASM;
  AddressSpaceMarker userASM;
  MTRand_int32 rng;

  static void idleLoopSetup(KernelProcessor*) __noreturn;

public:
  inline KernelProcessor(Scheduler& c = Context::CurrProcessor().getScheduler())
  : BaseProcessor(c), HardwareProcessor(this) {}
  void start(funcvoid0_t func) __noreturn;
  void seedRNG(mword s) { rng.seed(s); }
  mword random() { return rng(); }
};

#endif /* _KernelProcessor_h_ */
