/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "kernel/Process.h"
#include "world/Access.h"
#include "machine/HardwareProcessor.h"

#include "ulib/syscalls.h"
#include "ulib/pthread.h"

/******* libc functions *******/

// for C-style 'assert' (e.g., from malloc.c)
extern "C" void __assert_func( const char* const file, size_t line,
  const char* const func, const char* const expr ) {
  KERR::outl("ASSERT: ", file, ':', line, ' ', func, ' ', expr);
  Reboot();
}

extern "C" void abort() {
  RABORT0();
}

extern "C" void free(void* ptr) { KernelHeap::legacy_free(ptr); }
extern "C" void _free_r(_reent* r, void* ptr) { free(ptr); }
extern "C" void* malloc(size_t size) { return KernelHeap::legacy_malloc(size); }
extern "C" void* _malloc_r(_reent* r, size_t size) { return malloc(size); }
extern "C" void* realloc(void* ptr, size_t size) { return KernelHeap::legacy_realloc(ptr, size); }

extern "C" void* calloc(size_t nmemb, size_t size) {
  void* ptr = malloc(nmemb * size);
  memset(ptr, 0, nmemb * size);
  return ptr;
}

extern "C" void* _calloc_r(_reent* r, size_t nmemb, size_t size) {
  return calloc(nmemb, size);
}

extern "C" void* _realloc_r(_reent* r, void* ptr, size_t size) {
  return realloc(ptr, size);
}

/******* syscall functions *******/

// libc exit calls atexit routines, then invokes _exit
extern "C" void _exit(int result) {
  CurrProcess().exit(result);
  LocalProcessor::lock(true);
  CurrProcess().exitThread(nullptr);
}

extern "C" int open(const char *path, int oflag, ...) {
  Process& p = CurrProcess();
  auto it = kernelFS.find(path);
  if (it == kernelFS.end()) return -ENOENT;
  return p.ioHandles.store(knew<FileAccess>(it->second));
}

extern "C" int close(int fildes) {
  Process& p = CurrProcess();
  Access* access = p.ioHandles.removeWait(fildes);
  if (!access) return -EBADF;
  delete access;
  return 0;
}

extern "C" ssize_t read(int fildes, void* buf, size_t nbyte) {
  // TODO: validate buf/nbyte
  Process& p = CurrProcess();
  Access* access = p.ioHandles.startAccess(fildes);
  if (!access) return -EBADF;
  ssize_t ret = access->read((char*)buf, nbyte);
  p.ioHandles.stopAccess(fildes);
  return ret;
}

extern "C" ssize_t write(int fildes, const void* buf, size_t nbyte) {
  // TODO: validate buf/nbyte
  Process& p = CurrProcess();
  Access* access = p.ioHandles.startAccess(fildes);
  if (!access) return -EBADF;
  ssize_t ret = access->write((const char*)buf, nbyte);
  p.ioHandles.stopAccess(fildes);
  return ret;
}

extern "C" off_t lseek(int fildes, off_t offset, int whence) {
  Process& p = CurrProcess();
  Access* access = p.ioHandles.startAccess(fildes);
  if (!access) return -EBADF;
  ssize_t ret = access->lseek(offset, whence);
  p.ioHandles.stopAccess(fildes);
  return ret;
}

extern "C" int kill(pid_t pid, int sig) {
  return Process::kill(pid, sig);
}

extern "C" int waitpid(pid_t pid, int* wstatus, int options) {
  return Process::join(pid, *wstatus);
}

extern "C" pid_t getpid() {
  return CurrProcess().getID();
}

extern "C" pid_t getcid() {
  return LocalProcessor::getIndex();
}

extern "C" int usleep(useconds_t usecs) {
  sleepFred(Time::fromUS(usecs));
  return 0;
}

extern "C" int _mmap(void** addr, size_t len, int protflags, int fildes, off_t off) {
  // TODO: validate addr
#if TESTING_ENABLE_ASSERTIONS
  int prot = protflags & 0xf;
  int flags = protflags >> 4;
  RASSERT(prot == 0, prot);
  RASSERT(flags == 0, flags);
#endif
  RASSERT(fildes == -1, fildes);
  RASSERT(off == 0, off);
  vaddr va = CurrProcess().mmap<smallpl>(vaddr(*addr), len);
  if (va == topaddr) return -ENOMEM; // shouldn't happen currently...
  *addr = (void*)va;
  return 0;
}

extern "C" int _munmap(void* addr, size_t len) {
  CurrProcess().munmap<smallpl>(vaddr(addr), len);
  return 0;
}

extern "C" pthread_t _pthread_create(funcvoid2_t invoke, funcvoid1_t func, void* data) {
  return CurrProcess().startThread(invoke, func, data);
}

extern "C" int pthread_detach(pthread_t tid) {
  CurrProcess().detachThread(tid);
  return 0;
}

extern "C" void pthread_exit(void* result) {
  LocalProcessor::lock(true);
  CurrProcess().exitThread(result);
}

extern "C" int pthread_join(pthread_t tid, void** result) {
  // TODO: validate result
  return CurrProcess().joinThread(tid, *result);
}

extern "C" int pthread_kill(pthread_t tid, int signal) {
  return CurrProcess().signalThread(tid, signal);
}

extern "C" pthread_t pthread_self() {
  return Process::getCurrentThreadID();
}

extern "C" int pthread_yield() {
  Thread::yield();
  return 0;
}

extern "C" int semCreate(mword* rsid, mword init) {
  // TODO: validate rsid
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  LockedSemaphore<KernelLock>* s = knew<LockedSemaphore<KernelLock>>(init);
  *rsid = p.semStore.put(s);
  return 0;
}

extern "C" int semDestroy(mword sid) {
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  if (!p.semStore.valid(sid)) return -1;
  kdelete(p.semStore.access(sid));
  p.semStore.remove(sid);
  return 0;
}

extern "C" int semP(mword sid) {
  Process& p = CurrProcess();
  p.semLock.acquire();
  if (!p.semStore.valid(sid))  { p.semLock.release(); return -1; }
  p.semStore.access(sid)->unlockP(p.semLock);
  return 0;
}

extern "C" int semV(mword sid) {
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  if (!p.semStore.valid(sid)) return -1;
  p.semStore.access(sid)->V();
  return 0;
}

extern "C" int semTryP(mword sid) {
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  if (!p.semStore.valid(sid)) return -1;
  p.semStore.access(sid)->tryP();
  return 0;
}

extern "C" int semTryV(mword sid) {
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  if (!p.semStore.valid(sid)) return -1;
  p.semStore.access(sid)->tryV();
  return 0;
}

extern "C" int semUnlockP(mword sid, mword sid2) {
  Process& p = CurrProcess();
  p.semLock.acquire();
  if (!p.semStore.valid(sid))  { p.semLock.release(); return -1; }
  if (!p.semStore.valid(sid2)) { p.semLock.release(); return -1; }
  p.semStore.access(sid)->unlockP(p.semLock, *p.semStore.access(sid2));
  return 0;
}

typedef int (*funcint4_t)(mword, mword, mword, mword);
extern "C" int privilege(ptr_t func, mword a1, mword a2, mword a3, mword a4) {
  return ((funcint4_t)func)(a1, a2, a3, a4);
}

extern "C" void _init_sigtrampoline(vaddr sigtramp) {
  CurrProcess().setSignalTrampoline(sigtramp);
}

/******* dummy functions *******/

extern "C" int fstat(int fildes, struct stat *buf) {
  RABORT("fstat"); return -ENOSYS;
}

//extern "C" char *getenv(const char *name) {
//  DBG::outl(DBG::Libc, "LIBC/getenv: ", name);
//  return nullptr;
//}

extern "C" int isatty(int fd) {
  RABORT("isatty"); return -ENOSYS;
}

//extern "C" void* sbrk(intptr_t increment) {
//  RABORT("sbrk"); return (void*)-1;
//}

void* __dso_handle = nullptr;

typedef ssize_t (*syscall_t)(mword a1, mword a2, mword a3, mword a4, mword a5);
static const syscall_t syscalls[] = {
  syscall_t((void*)_exit),              // 0
  syscall_t((void*)open),               // 1
  syscall_t((void*)close),              // 2
  syscall_t((void*)read),               // 3
  syscall_t((void*)write),              // 4
  syscall_t((void*)lseek),              // 5
  syscall_t((void*)kill),               // 6
  syscall_t((void*)waitpid),            // 7
  syscall_t((void*)getpid),             // 8
  syscall_t((void*)getcid),             // 9
  syscall_t((void*)usleep),             // 10
  syscall_t((void*)_mmap),              // 11
  syscall_t((void*)_munmap),            // 12
  syscall_t((void*)_pthread_create),    // 13
  syscall_t((void*)pthread_detach),     // 14
  syscall_t((void*)pthread_exit),       // 15
  syscall_t((void*)pthread_join),       // 16
  syscall_t((void*)pthread_kill),       // 17
  syscall_t((void*)pthread_self),       // 18
  syscall_t((void*)pthread_yield),      // 19
  syscall_t((void*)semCreate),          // 20
  syscall_t((void*)semDestroy),         // 21
  syscall_t((void*)semP),               // 22
  syscall_t((void*)semV),               // 23
  syscall_t((void*)semTryP),            // 24
  syscall_t((void*)semTryV),            // 25
  syscall_t((void*)semUnlockP),         // 26
  syscall_t((void*)privilege),          // 27
  syscall_t((void*)_init_sigtrampoline) // 28
};

static_assert(sizeof(syscalls)/sizeof(syscall_t) == SyscallNum::max, "syscall list error");

extern "C" ssize_t syscall_handler(mword x, mword a1, mword a2, mword a3, mword a4, mword a5) {
  DBG::outl(DBG::Syscalls, "syscall: ", x, ' ', FmtHex(a1), ' ', FmtHex(a2), ' ', FmtHex(a3), ' ', FmtHex(a4), ' ', FmtHex(a5));
  if fastpath(x < SyscallNum::max) return syscalls[x](a1, a2, a3, a4, a5);
  DBG::outl(DBG::Tests, "unknown syscall: ", x);
  return -ENOSYS;
}

extern "C" mword syscall_signals() {
  return CurrProcess().checkSignals();
}
