/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Thread_h_
#define _Thread_h_ 1

#include "runtime/Fred.h"
#include "kernel/KernelProcessor.h"

class AddressSpace;

class Thread : public Fred {
  vaddr      stackBottom;   // bottom of allocated memory for thread/stack
  size_t     stackSize;     // size of allocated memory

  AddressSpace& as;

protected:
  Thread(vaddr sb, size_t ss, AddressSpace& as);
  Thread(KernelProcessor& p, vaddr sb, size_t ss, AddressSpace& as);
  ~Thread() = default;

public:
  // public thread creation interface
  static Thread* create(size_t ss = defaultStackSize);
  static Thread* create(KernelProcessor& p, size_t ss = defaultStackSize);
  // callback from Fred via Runtime after terminal context switch
  void destroy(_friend<Fred>);
  AddressSpace& getAS() { return as; }
};

static Thread* CurrThread() {
  Thread* t = reinterpret_cast<Thread*>(Context::CurrFred());
  RASSERT0(t);
  return t;
}

#endif /* _Thread_h_ */
