/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "kernel/FrameManager.h"
#include "machine/HardwareProcessor.h"
#include "machine/Paging.h"

template<size_t PL, char Type>
inline void FrameMap<PL,Type>::print(std::ostream& os, paddr base, size_t range) {
  ScopedLock<KernelMCSLock> sl(lock);
  size_t max = range / fsize;
  size_t idx = 0;
  size_t cnt;
  os << ' ' << Type << PL << ':';
  while ((cnt = frames.findrange(idx, max))) {
    os << ' ' << FmtHex(base + fsize * idx)
       << '-' << FmtHex(base + fsize * (idx+cnt));
    idx += cnt;
  }
  FrameMap<PL+1,Type>::print(os, base, range);
}

std::ostream& operator<<(std::ostream& os, const FrameManager& fm) {
  const_cast<FrameManager&>(fm).cleanFrames.print(os, fm.baseAddress, fm.memRange);
  const_cast<FrameManager&>(fm).dirtyFrames.print(os, fm.baseAddress, fm.memRange);
  return os;
}

void FrameZero(paddr pma, size_t size, _friend<FrameManager> _friend_fm) {
  RASSERT(size <= kernelps, FmtHex(size));
  DBG::outl(DBG::Frame, "FM/zero: ", FmtHex(pma), '/', FmtHex(size));
  size_t offset = pma % kernelps;
  vaddr vma = zeroBase + kernelps * LocalProcessor::getIndex();
  Paging::mapPage<kernelpl>(vma, pma - offset, Paging::Data, _friend_fm);
  memset((ptr_t)(vma + offset), 0, size);
  Paging::unmap<kernelpl>(vma, _friend_fm);
}
