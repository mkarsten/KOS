/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "kernel/AddressSpace.h"
#include "kernel/Thread.h"

Thread::Thread(vaddr sb, size_t sz, AddressSpace& as)
: Fred(Context::CurrProcessor().getScheduler()), stackBottom(sb), stackSize(sz), as(as) {
  initStackPointer(vaddr(this));
  DBG::outl(DBG::Threads, "Thread create: ", FmtHex(this), '/', FmtHex(stackBottom), '/', FmtHex(stackSize));
}

Thread::Thread(KernelProcessor& p, vaddr sb, size_t sz, AddressSpace& as)
: Fred(p), stackBottom(sb), stackSize(sz), as(as) {
  initStackPointer(vaddr(this));
  DBG::outl(DBG::Threads, "Thread create: ", FmtHex(this), '/', FmtHex(stackBottom), '/', FmtHex(stackSize));
}

Thread* Thread::create(size_t sz) {
  vaddr mem = kernelAS.allocStack(sz);
  vaddr This = mem + sz - sizeof(Thread);
  return new (ptr_t(This)) Thread(mem, sz, defaultAS);
}

Thread* Thread::create(KernelProcessor& p, size_t sz) {
  vaddr mem = kernelAS.allocStack(sz);
  vaddr This = mem + sz - sizeof(Thread);
  return new (ptr_t(This)) Thread(p, mem, sz, defaultAS);
}

void Thread::destroy(_friend<Fred>) {
  DBG::outl(DBG::Threads, "Thread destroy: ", FmtHex(this), '/', FmtHex(stackBottom), '/', FmtHex(stackSize));
  kernelAS.releaseStack(stackBottom, stackSize);
}

