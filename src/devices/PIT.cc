/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "machine/CPU.h"
#include "machine/Machine.h"
#include "devices/PIT.h"

static const mword bfreq3 = 3579545; // base frequency is 1193181.666 Hz

// http://www.jamesmolloy.co.uk/tutorial_html/5.-IRQs%20and%20the%20PIT.html
void PIT::init(mword hz, mword& eN, mword& eD) {
	// register PIT interrupts
	Machine::registerIrqSync(PIC::PIT, 0xf0);

	RASSERT(hz <= bfreq3 / 3, hz);
	mword divisor = divup(bfreq3, (3 * hz)); // make clock slow, rather than fast
	RASSERT(divisor < 65536, hz);
	eN = 3 * hz * divisor - bfreq3;
	eD = 3;

	uint16_t d = divisor;
	CPU::out8(0x43, 0x36);     // command: binary counting, mode 3, channel 0
	CPU::out8(0x40, d & 0xFF); // set frequency divisor LSB
	CPU::out8(0x40, d >> 8);   // set frequency divisor MSB
}
