#!/bin/bash

function killtest() {
	while kill $(pgrep -f "$1") 2>/dev/null; do sleep 1; wait; done
}

function runtest() {
	make $1 iso 2>&1
	echo -n > /tmp/KOS.serial
	make $1 all 2>&1
	timeout 300 make $1 $2 2>&1 &
	sleep 3
	case $2 in
		qemu)  pidstring="qemu-system-x86_64.*KOS";;
		vbox)  pidstring=".*/lib/.*VirtualBox.*KOS";;
		bochs) pidstring="bochs.*kos";;
		*)     echo $2; exit 1;;
	esac
	trap "killtest "$pidstring"; exit 1" EXIT
	while pgrep -f "$1" >/dev/null && ! grep -F -q "OUT OF MEMORY" /tmp/KOS.serial; do
		sleep 3;
	done
	killtest "$pidstring"
	grep -F -a "FP: signal 0x100000" /tmp/KOS.serial || return 1
	grep -F -a "FP mismatch" /tmp/KOS.serial && return 1
	grep -F -a "unknown syscall: 100" /tmp/KOS.serial || return 1
	grep -F -a "systest done" /tmp/KOS.serial || return 1
	if [ "$2" = "bochs" ]; then # bochs software emulation too slow to finish threadtest/foobar3
		grep -F -a "pokey 5" /tmp/KOS.serial || return 1
	else
		grep -F -a "foobar3 done" /tmp/KOS.serial || return 1
	fi
	return 0
}

if [ $# -ge 1 ]; then
	for target in ${*}; do
		echo RUNNING: "" "$target"
		runtest "" "$target"
	done
	exit 0
fi

outfile=$(mktemp -t testsuite.XXXXXX)
runfile=$(mktemp -t testsuite.run.XXXXXX)
echo OUTPUT in $runfile
echo SUMMARY in $outfile
for compile in gcc clang gccdebug clangdebug
do
	make clean 2>&1 >> $runfile
	case $compile in
		gcc)        flags="";;
		gccdebug)   flags="OPTIM=0";;
		clang)      flags="CC=clang";;
		clangdebug) flags="CC=clang OPTIM=0";;
		*)          echo $compile; exit 1;
	esac
	make $flags all 2>&1 >> $runfile
	for target in qemu vbox bochs
	do
		echo -n RUNNING: "$compile" "$flags" "$target - " | tee -a $outfile
		runtest "$flags" "$target" 2>&1 >> $runfile && echo SUCCESS | tee -a $outfile || echo FAILED | tee -a $outfile
		cp /tmp/KOS.serial /tmp/KOS.serial.$target.$compile
	done
done
trap - EXIT
echo "TESTSUITE FINISHED"
exit 0
