/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "machine/Machine.h"
#include "devices/RTC.h"

static inline constexpr uint8_t convert(uint8_t val, bool bcd) {
  return bcd ? (val / 16) * 10 + val % 16 : val;
}

void RTC::init(mword hz, mword& eN, mword& eD) { // see http://wiki.osdev.org/RTC
  // register RTC interrupts
  Machine::registerIrqSync(PIC::RTC, 0xf8);

  RASSERT(floorlog2(hz) == ceilinglog2(hz), hz);
  RASSERT(floorlog2(hz) < 16, hz);
  mword exp = 16 - floorlog2(hz);         // actual frequency at most 32768 hz
  eN = 0;
  eD = 0;

  CPU::out8(0x70, CPU::in8(0x70) | 0x80); // disable NMI
  write(0x0A, (read(0x0A) & 0xF0) | exp); // set rate to desired Hz
  write(0x0B, read(0x0B) | 0x40);         // enable RTC with periodic firing
  CPU::out8(0x70, CPU::in8(0x70) & 0x7F); // enable NMI

  staticInterruptHandler();               // needed to get interrupts going
}

time_t RTC::readCurrentTime() {
  // read current wall-clock time
  bool bcd = !(bool)(read(0x0B) & 0x04);  // check representation
  struct tm t;
  t.tm_yday  = 0;
  t.tm_isdst = 0;
  do { // possibly repeat read-out, if seconds flip from 59 to 0
    t.tm_sec  = convert(read(0x00), bcd);
    t.tm_min  = convert(read(0x02), bcd);
    t.tm_hour = convert(read(0x04), bcd);
    t.tm_mday = convert(read(0x07), bcd);
    t.tm_mon  = convert(read(0x08), bcd) - 1;
    t.tm_year = convert(read(0x09), bcd) + convert(read(0x32), bcd) * 100 - 1900;
    t.tm_wday = convert(read(0x06), bcd) - 1;
  } while (t.tm_sec == 59 && read(0x00) == 0);
  return mktime(&t);
}
