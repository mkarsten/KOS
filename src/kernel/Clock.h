/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Clock_h_
#define _Clock_h_ 1

#include <ctime>

class Clock {

  const mword hz;
  const long tickNSEC;
  const long tickSEC;

  volatile mword  currTick;
  volatile Time   currTime;

  mword errorNom;
  mword errorDenom;
  mword correction;

  friend inline std::ostream& operator<<(std::ostream&, const Clock&);
public:
  Clock(mword hz) : hz(hz), tickNSEC(Time::NSEC / hz), tickSEC(tickNSEC * hz),
                    currTick(0), currTime(0,0),
                    errorNom(0), errorDenom(0), correction(0) {}

  void init(mword ct, mword cN, mword cD) {
    currTime.tv_sec = ct;
    currTime.tv_nsec = 0;
    errorNom = cN;
    errorDenom = cD;
  }

  void ticker() {
    currTick += 1;
    currTime.tv_nsec += tickNSEC;
    if (currTime.tv_nsec >= tickSEC) {
      currTime.tv_sec += 1;
      currTime.tv_nsec = 0;
      correction += errorNom;
      while (correction > errorDenom * hz) {
        correction -= errorDenom * hz;
        currTick += 1;
        currTime.tv_nsec += tickNSEC;
      }
    }
  }

  mword curr() { return currTick; }

  void wait(mword ticks) {
    mword start = currTick;
    while (currTick < start + ticks) Pause();
  }

  Time now() { return currTime; }
};

inline std::ostream& operator<<(std::ostream& os, const Clock& c) {
  struct tm helper;
  char dummy[64];
  strftime(dummy, 64, "%a %b %d %H:%M:%S %Y", gmtime_r((time_t*)&c.currTime, &helper));
  os << dummy;
  return os;
}

extern Clock kernelClock;
static const mword kernelHZ = 1024;

#endif /* _Clock_h_ */
