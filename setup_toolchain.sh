#!/bin/bash
#
# see 'config' for installation directories...
#
# variables set in this file:
# - change $TMPDIR, $DLDIR and $PTDIR below, if necessary
# - $ULDIR ends up hardcoded in a search path in gcc - change, if necessary

TMPDIR=/tmp/kos
cd $(dirname $0)
DLDIR=$(pwd)/download
PTDIR=$(pwd)/patches
ULDIR=$(pwd)/src/ulib
source $(pwd)/config
cd -

BINUTILS=binutils-2.37   # GNU mirror
BOCHS=bochs-2.7          # http://bochs.sourceforge.net/
GCC=gcc-$GCCVER          # GNU mirror
GDB=gdb-11.1             # GNU mirror
GRUB=grub-2.06           # GNU mirror
NEWLIB=newlib-4.1.0      # http://sourceware.org/newlib/
QEMU=qemu-6.2.0          # http://www.qemu.org/

mkdir -p $TMPDIR
uname -s|grep -F -qi CYGWIN && ROOTEXEC="bash -c" || {
	distro=$(cat /etc/*-release|grep -F NAME|grep -F -v PRETTY_NAME|grep -F -v CODENAME|cut -f2 -d\")
	case $distro in
		Ubuntu) ROOTEXEC="sudo sh -c";;
		*)      ROOTEXEC="su -c";;
	esac
}
NPROC=$(nproc)

function error() {
	echo FATAL ERROR: $1
	exit 1
}

function unpack() {
	rm -rf $TMPDIR/$1 && cd $TMPDIR || error "$TMPDIR access"
	tar xaf $DLDIR/$1.$2 || error "$1 extract"
}

function applypatch() {
	dir=$TMPDIR/$1
	[ -f $PTDIR/$1.patch ] && {
		patch -d $dir -p1 < $PTDIR/$1.patch || error "$1 patch"
	}
}

function prebuild() {
	dir=$TMPDIR/$1-build
	rm -rf $dir && mkdir $dir && cd $dir || error "$dir access"
}

function build() {
	cd $TMPDIR/$1-build
	make -j $NPROC all || error "$1 build failed"
}

function install() {
	echo -n "Installing $1 - "
	$ROOTEXEC "make -C $TMPDIR/$1-build install && echo SUCCESS: $1 install"
}

function unpack_gcc() {
	rm -rf $TMPDIR/$GCC && mkdir $TMPDIR/$GCC && cd $TMPDIR/$GCC || error "$TMPDIR/$GCC access"
	# order important; code overlap binutils/gcc/newlib; depends on version mix
	tar xaf $DLDIR/$NEWLIB.tar.gz --strip-components 1 || error "$NEWLIB extract"
	tar xaf $DLDIR/$BINUTILS.tar.xz --strip-components 1 || error "$BINUTILS extract"
	tar xaf $DLDIR/$GCC.tar.xz --strip-components 1 || error "$GCC extract"
	sh $PTDIR/$NEWLIB.sh $TMPDIR/$GCC || error "$NEWLIB shell patch"
}

function build_kgcc() {
	unpack_gcc
	sh $PTDIR/$GCC-k.sh $TMPDIR/$GCC || error "$GCC shell patch"
	patch -p1 < $PTDIR/$GCC-k.patch
	prebuild $GCC
	../$GCC/configure --target=$TARGET --prefix=$KGCCDIR --enable-languages=c,c++\
		--with-newlib=yes --enable-lto --disable-nls --disable-threads --disable-tls\
		--disable-wchar_t --disable-libstdcxx-dual-abi || error "$GCC configure"
	# compile gcc first pass
	make -j $NPROC all || error "$GCC 1st pass"
	make -C $TARGET/newlib clean
	sed -i 's/^CFLAGS = /CFLAGS = -mcmodel=kernel /' $TARGET/newlib/Makefile
	make -C $TARGET/newlib -j $NPROC all || error "$GCC newlib"
	make -C $TARGET/libstdc++-v3 clean
	sed -i 's/^CFLAGS = /CFLAGS = -mcmodel=kernel -DKERNEL /' $TARGET/libstdc++-v3/Makefile
	sed -i 's/^CXXFLAGS = /CXXFLAGS = -mcmodel=kernel -DKERNEL /' $TARGET/libstdc++-v3/Makefile
	make -C $TARGET/libstdc++-v3 -j $NPROC all || error "$GCC libstdc++-v3"
	# compile gcc second pass
	make -j $NPROC all || error "$GCC 2nd pass"
}

function build_ugcc() {
	unpack_gcc
	sh $PTDIR/$NEWLIB-u.sh $TMPDIR/$GCC || error "$NEWLIB-u shell patch"
	sh $PTDIR/$GCC-u.sh $TMPDIR/$GCC $ULDIR || error "$GCC-u shell patch"
	patch -p1 < $PTDIR/$GCC-u.patch
	prebuild $GCC
	# most options simply taken from typical Linux compiler configuration, except newlib/nls
	../$GCC/configure --target=$TARGET --prefix=$UGCCDIR --enable-languages=c,c++\
	  --with-newlib=yes --enable-lto --disable-nls --enable-default-pie\
		--disable-libssp --enable-install-libiberty --enable-checking=release\
		--enable-default-ssp --enable-cet=auto || error "$GCC configure"
	build $GCC
}

function build_gdb() {
	unpack $GDB tar.xz
	applypatch $GDB
	prebuild $GDB
	../$GDB/configure --target=$TARGET --prefix=$GDBDIR --with-guile=no || error "$GDB configure"
	build $GDB
}

function build_grub() {
	unpack $GRUB tar.xz
	applypatch $GRUB
	prebuild $GRUB
	../$GRUB/configure --target=$TARGET --prefix=$GRUBDIR --disable-werror\
	--disable-device-mapper || error "$GRUB configure"
	build $GRUB
}

function build_bochs() {
	unpack $BOCHS tar.gz
	applypatch $BOCHS
	prebuild $BOCHS
	../$BOCHS/configure --enable-cpu-level=6 --enable-smp --enable-x86-64 --enable-debugger\
		--enable-monitor-mwait --enable-idle-hack --enable-all-optimizations\
		--enable-avx --enable-evex --enable-disasm --enable-pci --enable-usb\
		--disable-docbook --enable-logging --with-x --with-x11 --with-term\
		--prefix=$BOCHSDIR || error "$BOCHS configure"
	sed -i 's/LIBS =  -lm/LIBS = -pthread -lm/' Makefile
	mkdir -p iodev/network/slirp
	build $BOCHS
}

function build_qemu() {
	unpack $QEMU tar.xz
	applypatch $QEMU
	prebuild $QEMU
	../$QEMU/configure --target-list=x86_64-softmmu --prefix=$QEMUDIR\
	--disable-libiscsi --disable-vde --disable-usb-redir --enable-sdl\
	|| error "$QEMU configure"
	export ARFLAGS=rc
	build $QEMU
	unset ARFLAGS
}

while [ $# -gt 0 ]; do case "$1" in
bochs)
	build_bochs && install $BOCHS;;
kgcc)
	build_kgcc && install $GCC;;
ugcc)
	build_ugcc && install $GCC;;
gdb)
	build_gdb && install $GDB;;
grub)
	build_grub && install $GRUB;;
qemu)
	build_qemu && install $QEMU;;
*)
	echo unknown argument: $1
	echo available packages: bochs, ugcc, kgcc, gdb, grub, qemu;;
esac; shift; done
