/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "syscalls.h"
#include "pthread.h"

static void _pthread_start(void* (*func)(void*), void* data) {
  void* result = func(data);
  pthread_exit(result);
}

extern "C" int pthread_create(pthread_t*restrict tid, const pthread_attr_t*restrict attr, void* (*func)(void*), void*restrict data) {
  *tid = syscallWrap(SyscallNum::_pthread_create, mword(_pthread_start), mword(func), mword(data));
  return 0;
}

extern "C" int pthread_detach(pthread_t tid) {
  return syscallWrap(SyscallNum::pthread_detach, tid);
}

extern "C" void pthread_exit(void* result) {
  syscallWrap(SyscallNum::pthread_exit, mword(result));
}

extern "C" int pthread_join(pthread_t tid, void** result) {
  return syscallWrap(SyscallNum::pthread_join, tid, mword(result));
}

extern "C" int pthread_kill(pthread_t tid, int sig) {
  return syscallWrap(SyscallNum::pthread_kill, tid, sig);
}

extern "C" int pthread_cancel(pthread_t tid) {
  return syscallWrap(SyscallNum::pthread_kill, tid, SIGTERM);
}

extern "C" pthread_t pthread_self(void) {
  return syscallWrap(SyscallNum::pthread_self);
}

extern "C" int pthread_yield(void) {
  return syscallWrap(SyscallNum::pthread_yield);
}

extern "C" int semCreate(mword* rsid, mword init) {
  return syscallWrap(SyscallNum::semCreate, mword(rsid), init);
}

extern "C" int semDestroy(mword sid) {
  return syscallWrap(SyscallNum::semDestroy, sid);
}

extern "C" int semP(mword sid) {
  return syscallWrap(SyscallNum::semP, sid);
}

extern "C" int semV(mword sid) {
  return syscallWrap(SyscallNum::semV, sid);
}

extern "C" int semTryP(mword sid) {
  return syscallWrap(SyscallNum::semTryP, sid);
}

extern "C" int semTryV(mword sid) {
  return syscallWrap(SyscallNum::semTryV, sid);
}

extern "C" int semUnlockP(mword sid, mword sid2) {
  return syscallWrap(SyscallNum::semUnlockP, sid, sid2);
}

//extern "C" int pthread_cond_broadcast(pthread_cond_t* c) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

//extern "C" int pthread_cond_destroy(pthread_cond_t* c) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

//extern "C" int pthread_cond_init(pthread_cond_t*restrict c, const pthread_condattr_t*restrict) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

//extern "C" int pthread_cond_signal(pthread_cond_t* c) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

//extern "C" int pthread_cond_timedwait(pthread_cond_t*restrict c, pthread_mutex_t*restrict m, const struct timespec*restrict t) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

//extern "C" int pthread_cond_wait(pthread_cond_t*restrict c, pthread_mutex_t*restrict m) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

extern "C" int pthread_mutex_destroy(pthread_mutex_t* m) {
  return semDestroy(*m);
}

extern "C" int pthread_mutex_init(pthread_mutex_t*restrict m, const pthread_mutexattr_t*restrict a) {
  return semCreate(m, 1);
}

extern "C" int pthread_mutex_lock(pthread_mutex_t* m) {
  return semP(*m);
}

//extern "C" int pthread_mutex_timedlock(pthread_mutex_t*restrict m, const struct timespec*restrict t) {
//  *__errno() = ENOTSUP;
//  return -1;
//}

extern "C" int pthread_mutex_trylock(pthread_mutex_t* m) {
  return semTryP(*m);
}

extern "C" int pthread_mutex_unlock(pthread_mutex_t* m) {
  return semV(*m);
}
