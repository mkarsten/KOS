/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _KernelLocks_h_
#define _KernelLocks_h_ 1

#include "runtime/SpinLocks.h"
#include "runtime/ScopedLocks.h"
#include "machine/HardwareProcessor.h"

template<typename SpinLock>
class KernelSpinLock : protected SpinLock {
public:
  bool tryAcquire() {
    LocalProcessor::lock();
    if (SpinLock::tryAcquire()) return true;
    LocalProcessor::unlock();
    return false;
  }
  void acquire() {
    LocalProcessor::lock();
    SpinLock::acquire();
  }
  void acquireHalt() {
    LocalProcessor::lock<true>();
    SpinLock::acquire();
  }
  void release() {
    SpinLock::release();
    LocalProcessor::unlock();
  }
  void releaseHalt() {
    SpinLock::release();
    LocalProcessor::unlock<true>(true);
  }
};

template<>
class KernelSpinLock<BinaryOwnerLock<>> : protected BinaryOwnerLock<> {
public:
  mword tryAcquire() {
    LocalProcessor::lock();
    mword ret = BinaryOwnerLock::tryAcquire(LocalProcessor::getIndex());
    if (ret) return ret;
    LocalProcessor::unlock();
    return 0;
  }
  mword acquire(KernelSpinLock* l = nullptr) {
    LocalProcessor::lock();                  // LocalProcessor counts as well
    mword ret = BinaryOwnerLock::acquire(LocalProcessor::getIndex());
    if (l) l->release();
    return ret;
  }
  template<bool full = false>
  mword release() {
    mword ret = BinaryOwnerLock::release<full>(LocalProcessor::getIndex());
    LocalProcessor::unlock();                // LocalProcessor counts as well
    return ret;
  }
  template<bool full = false>
  mword releaseHalt() {
    mword ret = BinaryOwnerLock::release<full>(LocalProcessor::getIndex());
    LocalProcessor::unlock<true>(true);
    return ret;
  }
};

template<>
class KernelSpinLock<MCSLock> : protected MCSLock {
public:
  typedef MCSLock::Node Node;
  void acquire(Node& n) {
    LocalProcessor::lock();
    MCSLock::acquire(n);
  }
  void release(Node& n) {
    MCSLock::release(n);
    LocalProcessor::unlock();
  }
  void releaseHalt(Node& n) {
    MCSLock::release(n);
    LocalProcessor::unlock<true>(true);
  }
};

class KernelLockRW : public SpinLockRW {
public:
  bool tryAcquireRead() {
    LocalProcessor::lock();
    if (SpinLockRW::tryAcquireRead()) return true;
    LocalProcessor::unlock();
    return false;
  }
  void acquireRead() {
    LocalProcessor::lock();
    SpinLockRW::acquireRead();
  }
  bool tryAcquireWrite() {
    LocalProcessor::lock();
    if (SpinLockRW::tryAcquireWrite()) return true;
    LocalProcessor::unlock();
    return false;
  }
  void acquireWrite() {
    LocalProcessor::lock();
    SpinLockRW::acquireWrite();
  }
  void release() {
    SpinLockRW::release();
    LocalProcessor::unlock();
  }
};

class KernelLock       : public KernelSpinLock<BinaryLock<>> {};
class KernelTicketLock : public KernelSpinLock<TicketLock> {};
class KernelOwnerLock  : public KernelSpinLock<BinaryOwnerLock<>> {};
class KernelMCSLock    : public KernelSpinLock<MCSLock> {};

template <>
class ScopedLock<KernelMCSLock> {
  KernelMCSLock& lk;
  KernelMCSLock::Node ln;
public:
  ScopedLock(KernelMCSLock& lk) : lk(lk) { lk.acquire(ln); }
  ~ScopedLock() { lk.release(ln); }
};

template <>
class ScopedLock<LocalProcessor> {
public:
  ScopedLock() { LocalProcessor::lock(); }
  ~ScopedLock() { LocalProcessor::unlock(); }
};

template<typename T>
class _FakeQueue {
  T* waiter;
public:
  _FakeQueue() : waiter(nullptr) {}
  bool empty() { return waiter == nullptr; }
  void push_back(T& elem) { waiter = &elem; }
  T* pop_front() {
    T* ret = waiter;
    waiter = nullptr;
    return ret;
  }
};

template<bool Binary, typename Queue>
class KernelSemaphore {
  KernelLock lock;
  Queue queue;
  size_t counter;
  size_t signal;
public:
  KernelSemaphore(size_t c = 0) : counter(c), signal(0) {}

  bool P() {
    ScopedLock<KernelLock> sl(lock);
    if (counter < 1) {
      HardwareProcessor* proc = HardwareProcessor::self();
      queue.push_back(*proc);
      for (;;) {
        if (signal) {
          signal -= 1;
          return true;
        }
        lock.release();
        bool zero = HardwareProcessor::preHalt();
        lock.acquire();
        if (!zero) break;
      }
      proc->halt(lock, signal);
      signal -= 1;
    } else {
      counter -= 1;
    }
    return true;
  }

  bool tryP() {
    ScopedLock<KernelLock> sl(lock);
    if (counter < 1) return false;
    counter -= 1;
    return true;
  }

  void V() {
    lock.acquire();
    if (queue.empty()) {
      if (Binary) counter = 1;
      else counter += 1;
      lock.release();
    } else {
      HardwareProcessor* proc = queue.pop_front();
      signal += 1;
      lock.release();
      proc->sendWakeIPI();
    }
  }
};

typedef KernelSemaphore<true,_FakeQueue<HardwareProcessor>> CoreSemaphore;

#endif /* _SpinLock_h_ */
