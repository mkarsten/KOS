/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _SynchronizedArray_h_
#define _SynchronizedArray_h_ 1

#include "runtime/BlockingSync.h"
#include "kernel/ManagedArray.h"

template<typename Lock>
class SynchronizedElement {
  template <typename, template<typename> class, typename> friend class SynchronizedArray;
  Condition<> join;
  size_t refcount;
public:
  SynchronizedElement() : refcount(0) {}
};

// NOTE: T must be a pointer type, otherwise synchronization is not much help
template<typename T, template<typename> class Alloc, typename Lock>
class SynchronizedArray : protected ManagedArray<T,SynchronizedFlag::Invalid,Alloc> {
  Lock lock;
  using Baseclass = ManagedArray<T,SynchronizedFlag::Invalid,Alloc>;
public:
  SynchronizedArray(size_t n = 0) : Baseclass(n) {}

  size_t currentIndex() const { return Baseclass::currentIndex(); }

  size_t store(const T elem) {
    RASSERT0(elem);
    lock.acquire();
    size_t idx = Baseclass::reserveIndex();
    lock.release();
    Baseclass::set(idx, elem);
    return idx;
  };

  T startAccess(size_t idx) {
    ScopedLock<Lock> al(lock);
    if (!Baseclass::valid(idx)) return nullptr;
    T elem = Baseclass::access(idx);
    RASSERT0(elem);
    elem->SynchronizedElement<Lock>::refcount += 1;
    return elem;
  }

  void stopAccess(size_t idx) {
    ScopedLock<Lock> al(lock);
    RASSERT(Baseclass::valid(idx), idx);
    T elem = Baseclass::access(idx);
    RASSERT(elem && elem->SynchronizedElement<Lock>::refcount > 0, FmtHex(elem));
    elem->SynchronizedElement<Lock>::refcount -= 1;
    if (elem->SynchronizedElement<Lock>::refcount == 0) elem->SynchronizedElement<Lock>::join.signal();
  }

  T removeWait(size_t idx) {
    ScopedLock<Lock> al(lock);
    for (;;) {
      if (!Baseclass::valid(idx)) return nullptr;
      T elem = Baseclass::access(idx);
      RASSERT0(elem);
      if (elem->SynchronizedElement<Lock>::refcount == 0) {
        Baseclass::remove(idx);
        return elem;
      }
      elem->SynchronizedElement<Lock>::join.wait(lock);
      lock.acquire();
    }
  }

  T removeForce(size_t idx) {
    ScopedLock<Lock> al(lock);
    if (!Baseclass::valid(idx)) return nullptr;
    T elem = Baseclass::access(idx);
    RASSERT0(elem);
    Baseclass::remove(idx);
    elem->SynchronizedElement<Lock>::join.reset();
    return elem;
  }
};

#endif /* _SynchronizedArray_h_ */
