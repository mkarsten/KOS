/*
 * TcpTest.cc
 *
 *  Created on: 2014-04-01
 *      Author: behrooz
 */

#include "runtime/BlockingSync.h"
#include "kernel/Output.h"

#include "extern/lwip/lwip/src/include/lwip/sockets.h"
#include "extern/lwip/lwip/src/include/lwip/netdb.h"

#include <cstring>

#define USE_DNS 1

#if USE_DNS
static const char server[] = "www.google.com";
#else
static const char server[] = "172.217.164.228";
#endif

int TcpTest() {
  DBG::outl(DBG::Lwip,"TcpTest: sleeping to let DHCP settle");
  sleepFred(Time(5,0));
  DBG::outl(DBG::Lwip,"TcpTest: creating socket");
  int fd = lwip_socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0) {
    DBG::outl(DBG::Lwip,"TcpTest: cannot create socket");
    return -1;
  }

  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(80);

  DBG::outl(DBG::Lwip,"TcpTest: server ", server);
#if USE_DNS
  struct addrinfo  hint = { 0, AF_INET, SOCK_STREAM, 0, 0, nullptr, nullptr, nullptr };
  struct addrinfo* info = nullptr;
  if (lwip_getaddrinfo(server, nullptr, &hint, &info) < 0 || !info) {
    DBG::outl(DBG::Lwip,"TcpTest: cannot look up ", server);
    lwip_close(fd);
    return -1;
  }
  addr.sin_addr = ((sockaddr_in*)info->ai_addr)->sin_addr;
  lwip_freeaddrinfo(info);
#else
  ip_addr_t temp;
  if (ipaddr_aton(server, &temp) <= 0) {
    DBG::outl(DBG::Lwip,"TcpTest: cannot convert ", server);
    lwip_close(fd);
    return -1;
  }
  addr.sin_addr.s_addr = temp.addr;
#endif

  DBG::outl(DBG::Lwip,"TcpTest: connecting...");
  if (lwip_connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    DBG::outl(DBG::Lwip,"TcpTest: connect failed");
    lwip_close(fd);
    return -1;
  }

  DBG::outl(DBG::Lwip,"TcpTest: connected to ", server);

  const char *msg = "GET /index.html HTTP/1.0\n\n";
  lwip_write(fd,msg,strlen(msg));
  static char buffer[8192];
  int len = lwip_read(fd,buffer,8191);
  buffer[len] = '\0';
  DBG::outl(DBG::Lwip,"TcpTest: received: ",buffer);

  lwip_close(fd);
  return 0;
}
