/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _AddressSpace_h_
#define _AddressSpace_h_ 1

#include "kernel/FrameManager.h"
#include "kernel/KernelHeap.h"
#include "kernel/KernelProcessor.h"
#include "machine/Paging.h"

#include "extern/stl/mod_set"

class Thread;

// TODO: store shared & swapped virtual memory regions in separate data
// structures - checked during page fault (swapped) resp. unmap (shared)
class BaseAddressSpace : public Paging {
  BaseAddressSpace(const BaseAddressSpace&) = delete;            // no copy
  BaseAddressSpace& operator=(const BaseAddressSpace&) = delete; // no assignment

  struct UnmapDescriptor : public DoubleLink<UnmapDescriptor> {
    vaddr  vma;
    paddr  pma;
    size_t size;
    bool   alloc;
    mword  dummy; // sizeof(UnmapDescriptor) >= sizeof(ReleaseDescriptor)
    UnmapDescriptor(vaddr v, paddr p, size_t s, bool a)
      : vma(v), pma(p), size(s), alloc(a) {}
    UnmapDescriptor(const UnmapDescriptor&) = default;
    UnmapDescriptor& operator=(const UnmapDescriptor&) = default;
  };

  struct ReleaseDescriptor : public mod_set_elem<ReleaseDescriptor*> {
    vaddr start;
    vaddr end;
    ReleaseDescriptor(vaddr s, vaddr e) : start(s), end(e) {}
    ReleaseDescriptor(const ReleaseDescriptor&) = default;
    ReleaseDescriptor& operator=(const ReleaseDescriptor&) = default;
    bool operator<(const ReleaseDescriptor& u) { return start < u.start; }
  };

  // UnmapDescriptor is transformed into ReleaseDescriptor after async unmapping
  static_assert(sizeof(UnmapDescriptor) >= sizeof(ReleaseDescriptor), "type sizes");

protected:
  KernelLock ulock;             // lock protecting page invalidation data
  sword unmapEpoch;                               // unmap epoch counter
  IntrusiveList<AddressSpaceMarker>   markerList; // list of CPUs active in AS
  IntrusiveList<UnmapDescriptor>      unmapList;  // list of pages to unmap
  IntrusiveStdSet<ReleaseDescriptor*> releaseSet; // list of vm ranges to release
  HeapCache<sizeof(UnmapDescriptor)>  udCache;

  KernelLock vlock;             // lock protecting virtual address range
  vaddr mapBottom, mapStart, mapTop;

  enum MapCode { NoAlloc, Alloc, Guard, Lazy };

  BaseAddressSpace() : unmapEpoch(0), mapBottom(0), mapStart(0), mapTop(0) {}
  virtual ~BaseAddressSpace() {
    RASSERT0(markerList.empty());
    RASSERT0(unmapList.empty());
#if 0
    // TODO: FIXME - unmapList should be empty at this point
    UnmapDescriptor* ud = unmapList.front();
    while (ud != unmapList.edge()) {
      DBG::outl(DBG::VM, "BAS(", FmtHex(this), ")/destroy: ", FmtHex(ud->vma), '/', FmtHex(ud->size), " -> ", FmtHex(ud->pma));
      ud = IntrusiveList<UnmapDescriptor>::next(*ud);
    }
#endif
  }

  void setup(vaddr bot, vaddr top) {
    mapBottom = bot;
    mapStart = mapTop = top;
  }

  static void verifyPT(paddr pt) {
    RASSERT( pt == CPU::readCR3(), FmtHex(pt), ' ', FmtHex(CPU::readCR3()));
  }

  static constexpr MapCode AllocCode(PageType owner) {
#if TESTING_DISABLE_ALLOC_LAZY
    return Alloc;
#else
    return owner == Kernel ? Alloc : Lazy;
#endif
  }

  template<size_t N>
  vaddr getVmRange(vaddr addr, size_t& size) {
    RASSERT(mapBottom < mapTop, "no AS memory break set yet");
    vaddr end = (addr >= mapBottom) ? align_up(addr + size, pagesize<N>()) : align_down(mapStart, pagesize<N>());
    vaddr start = align_down(end - size, pagesize<N>());
    if (start < mapBottom) goto allocFailed;
    if (start < mapStart) mapStart = start;
    size = end - start;
    DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/get: ", FmtHex(start), '-', FmtHex(end));
    return start;
allocFailed:
    RABORT0();
  }

  void putVmRange(UnmapDescriptor* ud) {
    ReleaseDescriptor* rd = new (ud) ReleaseDescriptor(ud->vma, ud->vma + ud->size);
    releaseSet.insert(rd);
    ScopedLock<KernelLock> sl(vlock);
    do {
      auto iter = releaseSet.begin();
      rd = *iter;
      if (rd->start > mapStart) break;
      RASSERT(rd->start == mapStart, FmtHex(mapStart), ' ', FmtHex(rd->start));
      RASSERT(rd->end    > mapStart, FmtHex(mapStart), ' ', FmtHex(rd->end));
      DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/put: ", FmtHex(mapStart), '-', FmtHex(rd->end));
      mapStart = rd->end;
      releaseSet.erase(iter);
      udCache.deallocate(reinterpret_cast<UnmapDescriptor*>(rd));
      // TODO: clear page tables in range [mapStart...rd->end]
    } while (!releaseSet.empty());
  }

  template<size_t N, MapCode mc, PageType owner>
  void mapRegion( paddr pma, vaddr vma, size_t size, PageType type ) {
    static_assert( N > 0 && N < pagelevels, "page level template violation" );
    RASSERT( NoAlloc <= mc && mc <= Lazy, mc );
    RASSERT( aligned(vma,  pagesize<N>()), vma );
    RASSERT( aligned(size, pagesize<N>()), size );
    for (vaddr end = vma + size; vma < end; vma += pagesize<N>()) {
      if (mc == Alloc) pma = Context::CurrFM().allocFrame<N>();
      DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/map<", N, ',', mc, ">: ", FmtHex(vma), " -> ", FmtHex(pma), " flags:", Paging::PageEntryFmt(type));
      switch (mc) {
        case NoAlloc:
        case Alloc:   Paging::mapPage<N>(vma, pma, type | owner); break;
        case Guard:   Paging::mapToGuard<N>(vma); break;
        case Lazy:    Paging::mapToLazy<N>(vma); break;
        default:      RABORT0();
      }
      if (mc == NoAlloc) pma += pagesize<N>();
    }
  }

  template<size_t N, MapCode mc>
  void unmapRegion( vaddr vma, size_t size ) {
    static_assert( N > 0 && N < pagelevels, "page level template violation" );
    RASSERT( NoAlloc <= mc && mc <= Alloc, mc );
    RASSERT( aligned(vma, pagesize<N>()), vma );
    RASSERT( aligned(size, pagesize<N>()), size );
    for (vaddr end = vma + size; vma < end; vma += pagesize<N>()) {
      paddr pma = Paging::unmap<N,false>(vma); // TLB invalidated separately
      bool alloc = (mc == Alloc && pma != guardPage && pma != lazyPage);
      DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/post: ", FmtHex(vma), '/', FmtHex(pagesize<N>()), " -> ", FmtHex(pma), " epoch:", unmapEpoch);
      ScopedLock<KernelLock> sl(ulock);
      UnmapDescriptor* ud = new (udCache.allocate()) UnmapDescriptor(vma, pma, pagesize<N>(), alloc);
      unmapList.push_back(*ud);
      unmapEpoch += 1;
    }
  }

  template<size_t N, bool alloc, PageType owner>
  vaddr bmap(vaddr addr, size_t size, paddr pma) {
    ScopedLock<KernelLock> sl(vlock);
    addr = getVmRange<N>(addr, size);
    const MapCode mc = alloc ? AllocCode(owner) : NoAlloc;
    mapRegion<N,mc,owner>(pma, addr, size, Data);
    return addr;
  }

  template<PageType owner>
  vaddr ballocStack(size_t ss) {
    RASSERT(ss >= minimumStackSize, ss);
    size_t size = ss + stackGuardPage;
    ScopedLock<KernelLock> sl(vlock);
    vaddr vma = getVmRange<stackpl>(0, size);
    RASSERT(size == ss + stackGuardPage, ss, ' ', size);
    mapRegion<stackpl,Guard,owner>(0, vma, stackGuardPage, Data);
    vma += stackGuardPage;
    mapRegion<stackpl,AllocCode(owner),owner>(0, vma, ss, Data);
    return vma;
  }

  void enter(AddressSpaceMarker& marker) {
    marker.enterEpoch = unmapEpoch;
    markerList.push_back(marker);
  }

  template<bool invalidate>
  void leave(AddressSpaceMarker& marker) {
    RASSERT0(!markerList.empty());
    bool front = (&marker == markerList.front());
    IntrusiveList<AddressSpaceMarker>::remove(marker);

    // explicit TLB invalidation of TLB entries for pages removed since
    // 'enterEpoch'.  Invalidation iterates backwards -> easier!
    // kernelSpace pages are G(lobal), so TLBs not flushed during 'mov cr3'.
    if (invalidate) {
      UnmapDescriptor* ud = unmapList.back();
      for (sword e = unmapEpoch; e - marker.enterEpoch > 0; e -= 1) {
        RASSERT0(ud != unmapList.edge());
        DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/kinv: ", FmtHex(ud->vma), '/', FmtHex(ud->size), " -> ", FmtHex(ud->pma), " epoch:", e-1);
        CPU::InvTLB(ud->vma);
        ud = IntrusiveList<UnmapDescriptor>::prev(*ud);
      }
    }

    // All unmap entries between 'oldest' and 'second-oldest' CPU are
    // flushed from all TLBs and can be removed from list.
    if (front) {
      sword end = markerList.empty() ? unmapEpoch : markerList.front()->enterEpoch;
      DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/invlist: ", marker.enterEpoch, '-', end);
      for (sword e = marker.enterEpoch; end - e > 0; e += 1) {
        RASSERT0(!unmapList.empty());
        UnmapDescriptor* ud = unmapList.front();
        DBG::outl(DBG::VM, "AS(", FmtHex(this), ")/inv: ", FmtHex(ud->vma), '/', FmtHex(ud->size), " -> ", FmtHex(ud->pma), (ud->alloc ? "a" : ""), " epoch:", e);
        if (ud->alloc) Context::CurrFM().release(ud->pma, ud->size);
        IntrusiveList<UnmapDescriptor>::remove(*ud);
        putVmRange(ud);
      }
    }
  }

public:
  void setup(vaddr bot, vaddr top, _friend<Machine>) {
    setup(bot, top);
  }

  void initInvalidation(AddressSpaceMarker& marker) {
    ScopedLock<KernelLock> sl(ulock);
    if (markerList.empty()) {
      enter(marker);
      leave<true>(marker);
    }
    enter(marker);
  }

  template<size_t N, bool alloc=true>
  void munmap(vaddr addr, size_t size) {
    RASSERT(aligned(addr, pagesize<N>()), addr);
    size = align_up(size, pagesize<N>());
    unmapRegion<N,alloc ? Alloc : NoAlloc>(addr, size);
  }

  void releaseStack(vaddr vma, size_t ss) {
    unmapRegion<stackpl,Alloc>(vma - stackGuardPage, ss + stackGuardPage);
  }

  static bool tablefault(vaddr vma, uint64_t pff) {
    return Paging::mapTable<pagetablepl>(vma, pff, Context::CurrFM());
  }
};

class KernelAddressSpace : public BaseAddressSpace {
public:
  template<size_t N, bool alloc=true>
  vaddr mmap(vaddr addr, size_t size, paddr pma = topaddr) {
    return BaseAddressSpace::bmap<N,alloc,Kernel>(addr, size, pma);
  }

  vaddr allocStack(size_t ss) {
    return BaseAddressSpace::ballocStack<Kernel>(ss);
  }

  // allocate and map contiguous physical memory: device buffers -> set MMapIO?
  vaddr allocContig( size_t size, paddr align, paddr limit) {
    paddr pma = Context::CurrFM().allocRegion(size, align, limit);
    if (size < kernelps) return mmap<1,false>(0, size, pma);
    else return mmap<kernelpl,false>(0, size, pma);
  }

  void runInvalidation() {
    ScopedLock<KernelLock> sl(ulock);
    AddressSpaceMarker& marker = LocalProcessor::self()->kernASM;
    leave<true>(marker);
    enter(marker);
  }

};

extern KernelAddressSpace kernelAS;

class AddressSpace : public BaseAddressSpace {
protected:
  paddr pagetable;         // root page table *physical* address

public:
  AddressSpace(int) : pagetable(topaddr) {}
  AddressSpace() {
    pagetable = Context::CurrFM().allocFrame<pagetablepl>();
    Paging::clone(pagetable);
    DBG::outl(DBG::AddressSpaceDbg, "AS(", FmtHex(this), ")/cloned: ", FmtHex(pagetable));
    BaseAddressSpace::setup(usertop - kernelps, usertop);
  }

  void init(paddr p, _friend<Machine>) {
    RASSERT(pagetable == topaddr, FmtHex(pagetable));
    pagetable = p;
    setup(usertop);
  }

  void setup(vaddr bssEnd) {
    BaseAddressSpace::setup(bssEnd, usertop);
  }

  ~AddressSpace() {
    RASSERT0(pagetable != topaddr);
    DBG::outl(DBG::AddressSpaceDbg, "AS(", FmtHex(this), ")/destroy:", FmtHex(pagetable));
    RASSERT(pagetable != CPU::readCR3(), FmtHex(CPU::readCR3()));
    Context::CurrFM().release(pagetable, pagetableps);
  }

  void clean() {
    DBG::outl(DBG::AddressSpaceDbg, "AS(", FmtHex(this), ")/clean:", *this);
    verifyPT(pagetable);
    Paging::clear(align_down(userbot, pagesize<pagelevels>()), align_up(usertop, pagesize<pagelevels>()), Context::CurrFM());
  }

  template<size_t N, bool alloc=true>
  vaddr mmap(vaddr addr, size_t size, paddr pma = topaddr) {
    verifyPT(pagetable);
    return BaseAddressSpace::bmap<N,alloc,User>(addr, size, pma);
  }

  template<size_t N, bool alloc=true>
  void munmap(vaddr addr, size_t size) {
    verifyPT(pagetable);
    BaseAddressSpace::munmap<N,alloc>(addr, size);
  }

  vaddr allocStack(size_t ss) {
    verifyPT(pagetable);
    return BaseAddressSpace::ballocStack<User>(ss);
  }

  void releaseStack(vaddr vma, size_t ss) {
    verifyPT(pagetable);
    BaseAddressSpace::releaseStack(vma, ss);
  }

  bool pagefault(vaddr vma, uint64_t pff) {
    verifyPT(pagetable);
    return Paging::mapFromLazy(vma, Data | User, pff, Context::CurrFM());
  }

  // allocate memory and map to specific virtual address: ELF loading
  template<size_t N, bool check=true>
  void allocDirect( vaddr vma, size_t size, PageType t ) {
    RASSERT(!check || vma < mapBottom || vma > mapTop, vma);
    mapRegion<N,Alloc,User>(0, vma, size, t);
  }

  // map memory to specific virtual address: ELF loading
  template<size_t N, bool check=true>
  void mapDirect( paddr pma, vaddr vma, size_t size, PageType t ) {
    RASSERT(!check || vma < mapBottom || vma > mapTop, vma);
    mapRegion<N,NoAlloc,User>(pma, vma, size, t);
  }

  void switchTo(AddressSpace& nextAS) {
    RASSERT0(LocalProcessor::checkLock());
    RASSERT0(pagetable != topaddr);
    verifyPT(pagetable);
    AddressSpaceMarker& marker = LocalProcessor::self()->userASM;
    if (pagetable != nextAS.pagetable) {
      DBG::outl(DBG::AddressSpaceDbg, "AS(", FmtHex(this), ")/leave: ", unmapEpoch);
      ulock.acquire();
      leave<false>(marker);
      ulock.release();
      Paging::installPagetable(nextAS.pagetable);
      DBG::outl(DBG::AddressSpaceDbg, "AS(", FmtHex(this), ")/enter: ", nextAS.unmapEpoch);
      ScopedLock<KernelLock> sl(nextAS.ulock);
      nextAS.enter(marker);
    } else {
      ScopedLock<KernelLock> sl(ulock);
      leave<true>(marker);
      enter(marker);
    }
    kernelAS.runInvalidation();
  }

  virtual void preThreadSwitch(Thread&) {}
  virtual void postThreadSwitch(Thread&) {}
  virtual bool threadTerminated() { return false; }

  void print(std::ostream& os) const;
};

inline std::ostream& operator<<(std::ostream& os, const AddressSpace& as) {
  as.print(os);
  return os;
}

extern AddressSpace defaultAS;

#endif /* _AddressSpace_h_ */
