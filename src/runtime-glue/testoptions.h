// **** general options - testing

#define TESTING_DISABLE_HEAP_CACHE    1

// **** general options - safer execution

//#define TESTING_DISABLE_ALLOC_LAZY    1
//#define TESTING_DISABLE_DEEP_IDLE     1
//#define TESTING_DISABLE_PREEMPTION    1
//#define TESTING_REPORT_INTERRUPTS     1

// **** KOS console/serial output configuration

//#define TESTING_DEBUG_STDOUT          1
#define TESTING_STDOUT_DEBUG          1
#define TESTING_STDERR_DEBUG          1

// **** KOS-specific tests

//#define TESTING_KEYCODE_LOOP          1
//#define TESTING_LOCK_TEST             1
#define TESTING_TCP_TEST              1
#define TESTING_MEMORY_HOG            1
#define TESTING_PING_LOOP             1
//#define TESTING_TIMER_TEST            1
