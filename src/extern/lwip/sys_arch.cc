extern "C" {
#include "lwip/sys.h"
}

#include "kernel/Buffers.h"
#include "kernel/Clock.h"
#include "kernel/KernelHeap.h"
#include "kernel/MessageQueue.h"
#include "kernel/Thread.h"

// A lock that serializes all LWIP code
static KernelOwnerLock* lwipLock = nullptr;

typedef LockedSemaphore<KernelLock,true> BinarySemaphore;

extern "C" void sys_init(void) {
  lwipLock = knew<KernelOwnerLock>();
}

static inline BinarySemaphore* S(sys_sem_t *sem) { return reinterpret_cast<BinarySemaphore*>(*sem); }

extern "C" err_t sys_sem_new(sys_sem_t *sem, u8_t count) {
  *sem = knew<BinarySemaphore>(count);
  return ERR_OK;
}

extern "C" void sys_sem_free(sys_sem_t *sem) {
  kdelete(S(sem));
}

extern "C" void sys_sem_signal(sys_sem_t *sem) {
  S(sem)->V();
}

extern "C" u32_t sys_arch_sem_wait(sys_sem_t *sem, u32_t timeout) {
  if (timeout == 0) {
    if (!S(sem)->P()) RABORT("Semaphore::P failed");
    return 0;
  } else if (S(sem)->P(kernelClock.now() + Time::fromMS(timeout))) {
    return 0;
  } else {
    return SYS_ARCH_TIMEOUT;
  }
}

extern "C" int sys_sem_valid(sys_sem_t *sem) {
  return *sem != nullptr;
}

extern "C" void sys_sem_set_invalid(sys_sem_t *sem) {
  *sem = nullptr;
}

static inline FredMutex* M(sys_mutex_t *mutex) { return reinterpret_cast<FredMutex*>(*mutex); }

extern "C" err_t sys_mutex_new(sys_mutex_t *mutex) {
  *mutex = knew<FredMutex>();
  return ERR_OK;
}

extern "C" void sys_mutex_free(sys_mutex_t *mutex) {
  kdelete(M(mutex));
}

extern "C" void sys_mutex_lock(sys_mutex_t *mutex) {
  M(mutex)->acquire();
}

extern "C" void sys_mutex_unlock(sys_mutex_t *mutex) {
  M(mutex)->release();
}

extern "C" int sys_mutex_valid(sys_mutex_t *mutex) {
  return *mutex != nullptr;
}

extern "C" void sys_mutex_set_invalid(sys_mutex_t *mutex) {
  *mutex = nullptr;
}

typedef MessageQueue<RuntimeRingBuffer<void*,KernelAllocator<void*>>,KernelLock> LwipMsgQueue;
static inline LwipMsgQueue* MQ(sys_mbox_t *mbox) { return reinterpret_cast<LwipMsgQueue*>(*mbox); }

extern "C" err_t sys_mbox_new(sys_mbox_t *mbox, int size) {
  *mbox = knew<LwipMsgQueue>( std::max(size,128) );
  return ERR_OK;
}

extern "C" void sys_mbox_free(sys_mbox_t *mbox) {
  kdelete(MQ(mbox));
}

extern "C" void sys_mbox_post(sys_mbox_t *mbox, void *msg) {
  reinterpret_cast<LwipMsgQueue*>(*mbox)->send(msg);
}

extern "C" err_t sys_mbox_trypost(sys_mbox_t *mbox, void *msg) {
  if (MQ(mbox)->trySend(msg)) return ERR_OK;
  return ERR_MEM;
}

extern "C" err_t sys_mbox_trypost_fromisr(sys_mbox_t *mbox, void *msg) {
  if (MQ(mbox)->trySend(msg)) return ERR_OK;
  return ERR_MEM;
}

extern "C" u32_t sys_arch_mbox_fetch(sys_mbox_t *mbox, void **msg, u32_t timeout) {
  if (timeout == 0) {
    if (!MQ(mbox)->recv(*msg)) RABORT("MessageQueue::recv failed");
    return 0;
  } else if (MQ(mbox)->recv(*msg, kernelClock.now() + Time::fromMS(timeout))) {
    return 0;
  } else {
    return SYS_ARCH_TIMEOUT;
  }
}

extern "C" u32_t sys_arch_mbox_tryfetch(sys_mbox_t *mbox, void **msg) {
  if (MQ(mbox)->tryRecv(*msg)) {
    return 0;
  } else {
    return SYS_ARCH_TIMEOUT;
  }
}

extern "C" int sys_mbox_valid(sys_mbox_t *mbox) {
  return *mbox != nullptr;
}

extern "C" void sys_mbox_set_invalid(sys_mbox_t *mbox) {
  *mbox = nullptr;
}

extern "C" sys_thread_t sys_thread_new(const char *name, lwip_thread_fn thread, void *arg, int stacksize, int prio) {
  Thread* t = Thread::create(stacksize + defaultStackSize);
  //  would need to define various THREAD_PRIO settings in opt.h
  // t->setPriority(prio);
  t->start((ptr_t)thread, arg);
  return t;
}

extern "C" sys_prot_t sys_arch_protect(void) {
  return lwipLock->acquire() - 1;
}

extern "C" void sys_arch_unprotect(sys_prot_t pval) {
  lwipLock->release();
}

extern "C" u32_t sys_now(void) {
 return kernelClock.now().toMS();
} 

extern "C" u32_t sys_jiffies(void) {
 RABORT0();
 return 0;
}

extern "C" void lwip_assert(const char* const loc, int line, const char* const func, const char* const msg) {
  Runtime::Assert::lock();
  Runtime::Assert::print(loc, line, func, msg);
  Runtime::Assert::printl();
  Runtime::Assert::unlock();
}

extern "C" void lwip_printf(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  ExternDebugPrintf(DBG::Lwip, fmt, args);
  va_end(args);
}

extern "C" int lwip_random() {
  return LocalProcessor::self()->random();
}
 