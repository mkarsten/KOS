/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "kernel/Output.h"
#include "devices/Screen.h"
#include "devices/Serial.h"

class ScreenBuffer : public OutputBuffer<char> {
  ScreenSegment segment;
protected:
  virtual std::streamsize xsputn(const char* s, std::streamsize n) {
    for (std::streamsize i = 0; i < n; i += 1) segment.write(s[i]);
    return n;
  }
public:
  ScreenBuffer(int firstline, int lastline, int startline = 0)
  : segment(firstline, lastline, startline) {}
};

static ScreenBuffer top_screen( 1, 20, 2 );
static ScreenBuffer bot_screen( 21, 25 );

class DebugBuffer : public OutputBuffer<char> {
protected:
  virtual std::streamsize xsputn(const char* s, std::streamsize n) {
    for (std::streamsize i = 0; i < n; i += 1) DebugDevice::write(s[i]);
    return n;
  }
};

static DebugBuffer dbg_buffer;

KernelOutput StdOut(top_screen);
KernelOutput StdErr(bot_screen);
KernelOutput StdDbg(dbg_buffer);

namespace Runtime {
  namespace Assert {
    void lock() {
      StdErr.lock();
      StdDbg.lock();
    }
    void unlock() {
      StdErr.unlock();
      StdDbg.unlock();
    }
    void abort() {
      Reboot();
    }
    void print1(sword x) {
      StdErr.print<false>(x);
      StdDbg.print<false>(x);
    }
    void print1(const char* x) {
      StdErr.print<false>(x);
      StdDbg.print<false>(x);
    }
    void print1(const FmtHex& x) {
      StdErr.print<false>(x);
      StdDbg.print<false>(x);
    }
    void printl() {
      StdErr.print<false>(kendl);
      StdDbg.print<false>(kendl);
    }
  }
}
