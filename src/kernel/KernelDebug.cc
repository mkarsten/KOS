/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Debug.h"
#include "kernel/KernelHeap.h"

static const char* DebugOptions[] = {
  "acpi",
  "addressSpace",
  "basic",
  "blocking",
  "boot",
  "cdi",
  "devices",
  "error",
  "frame",
  "file",
  "gdbdebug",
  "gdbenable",
  "idle",
  "kmem",
  "libc",
  "lwip",
  "memacpi",
  "paging",
  "perf",
  "pci",
  "process",
  "scheduling",
  "syscalls",
  "tests",
  "threads",
  "vm",
  "warning",
};

static_assert(sizeof(DebugOptions)/sizeof(char*) == DBG::MaxLevel, "debug options mismatch");

void DebugInit(char* dstring, bool print) {
  DBG::init(DebugOptions, dstring, print);
}

void ExternDebugPrintf(DBG::Level c, const char* fmt, va_list args) {
  va_list tmpargs;
  va_copy(tmpargs, args);
  int size = vsnprintf(nullptr, 0, fmt, tmpargs);
  va_end(tmpargs);
  if (size < 0) return;
  size += 1;
  char* buffer = knewN<char>(size);
  vsnprintf(buffer, size, fmt, args);
  DBG::out1(c, buffer);
  kdelete(buffer, size);
}
