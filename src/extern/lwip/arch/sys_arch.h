#ifndef _LWIP_ARCH_SYS_ARCH_H_
#define _LWIP_ARCH_SYS_ARCH_H_ 1

typedef void* sys_sem_t;
typedef void* sys_mutex_t;
typedef void* sys_mbox_t;
typedef void* sys_thread_t;

typedef int sys_prot_t;

#endif /* _LWIP_ARCH_SYS_ARCH_H_ */
