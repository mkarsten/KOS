/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RuntimeContext_h_
#define _RuntimeContext_h_ 1

#include "machine/HardwareProcessor.h"

namespace Context {
  // CurrFred() and CurrProcessor() needed for generic runtime code
  static inline Fred* CurrFred() {
    Fred* s = LocalProcessor::getCurrFred();
    RASSERT0(s);
    return s;
  }
  static inline KernelProcessor& CurrProcessor() {
    KernelProcessor* p = LocalProcessor::self();
    RASSERT0(p);
    return *p;
  }
  // CurrFM() only needed in KOS
  static inline FrameManager& CurrFM() {
    FrameManager* fm = LocalProcessor::getCurrFM();
    RASSERT0(fm);
    return *fm;
  }
}

#endif /* _RuntimeContext_h_ */
