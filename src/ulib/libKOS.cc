/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "syscalls.h"

#include <cstring>

/* symbols & init routine for global static constructors */
extern "C" void (*__preinit_array_start []) (void) __attribute__((weak));
extern "C" void (*__preinit_array_end []) (void) __attribute__((weak));
extern "C" void (*__init_array_start []) (void) __attribute__((weak));
extern "C" void (*__init_array_end []) (void) __attribute__((weak));
extern "C" void _init();

extern "C" void __libc_init_array() {
  size_t count = __preinit_array_end - __preinit_array_start;
  for (size_t i = 0; i < count; i++) __preinit_array_start[i]();
  _init();
  count = __init_array_end - __init_array_start;
  for (size_t i = 0; i < count; i++) __init_array_start[i]();
}

/* signal handling */
void (*_user_sighandler)(int s) = nullptr;
static volatile mword sigPending = 0;

static mword checkSig(mword sig) {
  __atomic_or_fetch(&sigPending, sig, __ATOMIC_RELAXED);
  if (_user_sighandler) {
    return __atomic_exchange_n(&sigPending, 0, __ATOMIC_RELAXED);
  } else {
    return 0;
  }
}

/* handler called after kernel preemption */
extern "C" void _KOS_sighandlerISR(mword sig) __attribute__((force_align_arg_pointer));
extern "C" void _KOS_sighandlerISR(mword sig) {
  sig = checkSig(sig);
  if (sig) {
    FloatingPointContext fp;
    fp.save();
    FloatingPointContext::initCPU();
    _user_sighandler(sig);
    fp.restore();
  }
}

/* handler called after system calls */
extern "C" void _KOS_sighandlerSYSCALL(mword sig) {
  sig = checkSig(sig);
  if (sig) {
    FloatingPointFlags().restore();
    _user_sighandler(sig);
  }
}

/* signal trampoline - used for async on-stack delivery from kernel after preemption */
extern "C" void _KOS_sigtrampoline();

/* system library global initialization */
extern "C" void _initialize_KOS_standard_library() {
  syscallStub(SyscallNum::_init_sigtrampoline, (mword)_KOS_sigtrampoline);
}

/* system calls */
extern "C" void abort() { _exit(0xdeadbeef); }

extern "C" void _free_r(_reent* r, void* ptr) { free(ptr); }
extern "C" void* _malloc_r(_reent* r, size_t size) { return malloc(size); }
extern "C" void* _calloc_r(_reent* r, size_t nmemb, size_t size) { return calloc(nmemb, size); }
extern "C" void* _realloc_r(_reent* r, void* ptr, size_t size) { return realloc(ptr, size); }

extern "C" void _exit(int) {
  syscallStub(SyscallNum::_exit);
  unreachable();
}

extern "C" int open(const char *path, int oflag, ...) {
  ssize_t ret = syscallWrap(SyscallNum::open, mword(path), oflag);
  if (ret < 0) { *__errno() = -ret; return -1; } else return ret;
}

extern "C" int close(int fildes) {
  ssize_t ret = syscallWrap(SyscallNum::close, fildes);
  if (ret < 0) { *__errno() = -ret; return -1; } else return ret;
}

extern "C" ssize_t read(int fildes, void* buf, size_t nbyte) {
  ssize_t ret = syscallWrap(SyscallNum::read, fildes, mword(buf), nbyte);
  if (ret < 0) { *__errno() = -ret; return -1; } else return ret;
}

extern "C" ssize_t write(int fildes, const void* buf, size_t nbyte) {
  if (fildes == STDOUT_FILENO) {                      // copy stdout to stddbg
    syscallWrap(SyscallNum::write, STDDBG_FILENO, mword(buf), nbyte);
  }
  ssize_t ret = syscallWrap(SyscallNum::write, fildes, mword(buf), nbyte);
  if (ret < 0) { *__errno() = -ret; return -1; } else return ret;
}

extern "C" off_t lseek(int fildes, off_t offset, int whence) {
  ssize_t ret = syscallWrap(SyscallNum::lseek, fildes, offset, whence);
  if (ret < 0) { *__errno() = -ret; return -1; } else return ret;
}

extern "C" int kill(pid_t pid, int sig) {
  return syscallWrap(SyscallNum::kill, pid, sig);
}

extern "C" int waitpid(pid_t pid, int* wstatus, int options) {
  return syscallWrap(SyscallNum::waitpid, pid, (mword)wstatus, (mword)options);
}

extern "C" pid_t getpid() {
  return syscallWrap(SyscallNum::getpid);
}

extern "C" pid_t getcid() {
  return syscallWrap(SyscallNum::getcid);
}

extern "C" int usleep(useconds_t usecs) {
  return syscallWrap(SyscallNum::usleep, usecs);
}

extern "C" void* mmap(void* addr, size_t len, int prot, int flags, int filedes, off_t off) {
  void* newaddr = addr;
  ssize_t ret = syscallWrap(SyscallNum::_mmap, mword(&newaddr), len, prot|(flags<<4), filedes, off);
  if (ret < 0) { *__errno() = -ret; return MAP_FAILED; } else return newaddr;
}

extern "C" int munmap(void* addr, size_t len) {
  ssize_t ret = syscallWrap(SyscallNum::_munmap, mword(addr), len);
  if (ret < 0) { *__errno() = -ret; return -1; } else return ret;
}

extern "C" int privilege(void* func, mword a1, mword a2, mword a3, mword a4) {
  return syscallWrap(SyscallNum::privilege, (mword)func, a1, a2, a3, a4);
}

/******* dummy functions *******/

extern "C" int fstat(int fildes, struct stat *buf) {
  memset(buf, 0, sizeof(struct stat));
  switch (fildes) {
    case STDIN_FILENO:  buf->st_mode = S_IFCHR;
    case STDOUT_FILENO: buf->st_mode = S_IFCHR;
    case STDERR_FILENO: buf->st_mode = S_IFCHR;
    default: buf->st_mode = S_IFREG;
  }
  return 0;
}

extern "C" char *getenv(const char *name) {
  return nullptr;
}

extern "C" int isatty(int fildes) {
  switch (fildes) {
    case STDIN_FILENO:  return 1;
    case STDOUT_FILENO: return 1;
    case STDERR_FILENO: return 1;
    default: return 0;
  }
}

extern "C" int gettimeofday(struct timeval * tp, void * tzp) {
  (void)tp;
  (void)tzp;
  return -1;
}
