/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _MessageQueue_h_
#define _MessageQueue_h_ 1

#include "runtime/BlockingSync.h"

template<typename Buffer, typename Lock>
class MessageQueue {
  typedef typename Buffer::Element Element;
  Lock lock;
  Buffer buffer;
  size_t sendSlots;                                  // used for baton-passing
  size_t recvSlots;                                  // used for baton-passing
  BlockingQueue sendQueue;
  BlockingQueue recvQueue;

  template<typename... Args>
  bool internalSend(const Element& elem, const Args&... args) {
    lock.acquire();
    if (sendSlots == 0) {
      if (!sendQueue.block(lock, args...)) return false;
      lock.acquire();
    } else {
      sendSlots -= 1;
    }
    buffer.push(elem);
    if (!recvQueue.unblock<true>()) recvSlots += 1; // try baton passing
    lock.release();
    return true;
  }

  template<typename... Args>
  bool internalRecv(Element& elem, const Args&... args) {
    lock.acquire();
    if (recvSlots == 0) {
      if (!recvQueue.block(lock, args...)) return false;
      lock.acquire();
    } else {
      recvSlots -= 1;
    }
    elem = buffer.front();
    buffer.pop();
    if (!sendQueue.unblock<true>()) sendSlots += 1; // try baton passing
    lock.release();
    return true;
  }

public:
  explicit MessageQueue(size_t N = 0) : buffer(N),
    sendSlots(buffer.max_size()), recvSlots(0) {}

  ~MessageQueue() {
    RASSERT0(buffer.empty());
    RASSERT(sendSlots == buffer.max_size(), sendSlots);
    RASSERT(recvSlots == 0, recvSlots);
  }

  size_t size() { return buffer.size(); }

  template<typename... Args>
  bool send(const Element& elem, const Args&... args) { return internalSend(elem, args...); }
  bool trySend(const Element& elem) { return send(elem, false); }

  template<typename... Args>
  bool recv(Element& elem, const Args&... args) { return internalRecv(elem, args...); }
  bool tryRecv(Element& elem) { return recv(elem, false); }

  Element recv() {
    Element e = Element();
    recv(e);
    return e;
  }
};

#endif /* _MessageQueue_h_ */
