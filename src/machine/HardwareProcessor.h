/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _HardwareProcessor_h_
#define _HardwareProcessor_h_ 1

#include "machine/asmdecl.h"
#include "machine/asmshare.h"
#include "machine/APIC.h"
#include "machine/CPU.h"
#include "machine/Descriptors.h"
#include "machine/Memory.h" // apicAddr, ioApicAddr

class FrameManager;
class Fred;
class KernelLock;
class KernelProcessor;

// defined in Machine.cc
void Reboot(vaddr = 0) __noreturn;
void RebootDirect() __noreturn;

static APIC*   MappedAPIC()   { return   (APIC*)apicAddr; }
static IOAPIC* MappedIOAPIC() { return (IOAPIC*)ioApicAddr; }

class HardwareProcessor {
  friend class LocalProcessor;      // member offsets for %gs-based access
  friend class Machine;             // init and setup routines

  mword            index;
  mword            apicID;
  mword            systemID;
  mword            lockCount;
  Fred*            currFred;
  KernelProcessor* currProc;
  FrameManager*    currFM;
  TaskStateSegment tss;

  /* task state segment: kernel stack for interrupts/exceptions */
  static const unsigned int nmiIST = 1;
  static const unsigned int dbfIST = 2;
  static const unsigned int stfIST = 3;

  // layout for syscall/sysret, because of (strange) rules for SYSCALL_LSTAR
  // SYSCALL_LSTAR essentially forces userCS = userDS + 1
  // layout for sysenter/sysexit would be: kernCS, kernDS, userCS, userDS
  // also: kernDS = 2 consistent with convention and segment setup in boot.S
  static const unsigned int kernCS  = 1;
  static const unsigned int kernDS  = 2;
  static const unsigned int userDS  = 3;
  static const unsigned int userCS  = 4;
  static const unsigned int tssSel  = 5; // TSS entry uses 2 entries
  static const unsigned int maxGDT  = 7;
  SegmentDescriptor gdt[maxGDT];

  bool idleTimerIRQ;

  void setupGDT(uint32_t n, uint32_t dpl, bool code)    __section(".boot.text");
  void setupTSS(uint32_t num, paddr addr)               __section(".boot.text");

  HardwareProcessor(const HardwareProcessor&) = delete;            // no copy
  HardwareProcessor& operator=(const HardwareProcessor&) = delete; // no assignment

  void install() {
    MSR::write(MSR::GS_BASE, mword(this)); // store 'this' in gs
    MSR::write(MSR::KERNEL_GS_BASE, 0);    // later: store user value in shadow gs
  }

  void setup(mword i, mword a, mword s, FrameManager& fm) {
    index = i;
    apicID = a;
    systemID = s;
    currFM = &fm;
  }

  void enableIdleTimerIRQ() { idleTimerIRQ = true; }

  void init(paddr, InterruptDescriptor*, size_t, bool)  __section(".boot.text");

protected:
  void setupCurrFred(Fred* s) { currFred = s; }

public:
  HardwareProcessor(KernelProcessor* s) : index(0), apicID(0), systemID(0),
    lockCount(1), currFred(nullptr), currProc(s), currFM(nullptr) {
#if TESTING_DISABLE_DEEP_IDLE
    idleTimerIRQ = true;
#else
    idleTimerIRQ = false;
#endif
  }
  mword getIndex() const { return index; }
  static HardwareProcessor* self();
  static bool preHalt();
  void halt(KernelLock& lock, volatile size_t& signal);
  void sendIPI(uint8_t vec) { MappedAPIC()->sendIPI(apicID, vec); }
  void sendWakeIPI() { sendIPI(APIC::WakeIPI); }
  void sendPreemptIPI() { sendIPI(APIC::PreemptIPI); }
} __packed __caligned;

class LocalProcessor {
  template<bool halt=false>
  static void enableInterrupts() {
    asm volatile("sti" ::: "memory");
    if (halt) asm volatile("hlt" ::: "memory"); // sti;hlt atomic on Intel/AMD
  }
  static void disableInterrupts() {
    asm volatile("cli" ::: "memory");
  }
  static void incLockCount() {
    asm volatile("addq $1, %%gs:%c0" :: "i"(offsetof(HardwareProcessor, lockCount)) : "cc", "memory");
  }
  static void decLockCount() {
    asm volatile("subq $1, %%gs:%c0" :: "i"(offsetof(HardwareProcessor, lockCount)) : "cc", "memory");
  }

  template<typename T, mword offset> static T get() {
    T x;
    asm volatile("movq %%gs:%c1, %0" : "=r"(x) : "i"(offset));
    return x;
  }

  template<typename T, mword offset> static void set(T x) {
    asm volatile("movq %0, %%gs:%c1" :: "r"(x), "i"(offset) : "memory");
  }

public:
  static void configInterrupts(bool irqs) {
    MappedAPIC()->setFlatMode();         // set flat logical destination mode
    MappedAPIC()->setLogicalDest(irqs ? 0x01 : 0x00); // join irq group
    MappedAPIC()->setTaskPriority(0x00); // accept all interrupts
    MappedAPIC()->enable(0xff);          // enable APIC, set spurious IRQ to 0xff
  }

  static mword getIndex() {
    return get<mword, offsetof(HardwareProcessor, index)>();
  }
  static mword getApicID() {
    return get<mword, offsetof(HardwareProcessor, apicID)>();
  }
  static mword getSystemID() {
    return get<mword, offsetof(HardwareProcessor, systemID)>();
  }

  static mword getLockCount() {
    return get<mword, offsetof(HardwareProcessor, lockCount)>();
  }
  static mword checkLock() {
    RASSERT(CPU::interruptsDisabled() == bool(getLockCount()), getLockCount());
    return getLockCount();
  }
  static void lockFake() {
    RASSERT0(CPU::interruptsDisabled());
    incLockCount();
  }
  static void unlockFake() {
    RASSERT0(CPU::interruptsDisabled());
    decLockCount();
  }
  template<bool halt=false>
  static void lock(bool check = false) {
    RASSERT(!check || (checkLock() == 0), getLockCount());
    // despite looking like trouble, I believe this is safe: race could cause
    // multiple calls to disableInterrupts(), but this is no problem!
    if (halt || (getLockCount() == 0)) disableInterrupts();
    incLockCount();
  }
  template<bool halt=false>
  static void unlock(bool check = false) {
    if (halt) {
      RASSERT(!check || (checkLock() >= 1), getLockCount());
      decLockCount();
      enableInterrupts<halt>();
    } else {
      RASSERT(!check || (checkLock() == 1), getLockCount());
      decLockCount();
      // no races here (interrupts disabled)
      if (getLockCount() == 0) enableInterrupts<halt>();
    }
  }

  static Fred* getCurrFred() {
    return get<Fred*, offsetof(HardwareProcessor, currFred)>();
  }
  static void setCurrFred(Fred* x, _friend<Fred>) {
    set<Fred*, offsetof(HardwareProcessor, currFred)>(x);
  }
  static FrameManager* getCurrFM() {
    return get<FrameManager*, offsetof(HardwareProcessor, currFM)>();
  }

  static KernelProcessor* self() {
    return get<KernelProcessor*, offsetof(HardwareProcessor, currProc)>();
  }
  static void setKernelStack(Fred* currFred) {
    const mword o = offsetof(HardwareProcessor, tss) + offsetof(TaskStateSegment, rsp);
    static_assert(o == TSSRSP, "TSSRSP");
    set<Fred*, o>(currFred);     // Fred* = top of stack
  }
};

#endif /* HardwareProcessor_h_ */
