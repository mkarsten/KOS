/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _syscalls_h_
#define _syscalls_h_ 1

#include "runtime/Platform.h"
#include "runtime/FloatingPoint.h"

#include <cerrno>
#include <cstdlib>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#define STDDBG_FILENO 3

static const mword SIGTERM = mword(1) << 15;
static const mword SIGTEST = mword(1) << 20;

extern void (*_user_sighandler)(int s);

#define MAP_FAILED  ((void *) -1)
extern "C" void* mmap(void* addr, size_t len, int prot, int flags, int filedes, off_t off);
extern "C" int munmap(void* addr, size_t len);

extern "C" pid_t getcid();

extern "C" int privilege(void*, mword, mword, mword, mword);

namespace SyscallNum {

// Need to update 4 places for each syscall:
// 1) this enum list below
// 2) syscalls[] array at the end of kernel/syscalls.cc
// 3) kernel-side implementation in kernel/syscalls.cc
// 4) user-side implementation in ulib/libKOS.cc or ulib/pthread.cc

enum : mword {
  _exit = 0,
  open,
  close,
  read,
  write,
  lseek,
  kill,
  waitpid,
  getpid,
  getcid,
  usleep,
  _mmap,
  _munmap,
  _pthread_create,
  pthread_detach,
  pthread_exit,
  pthread_join,
  pthread_kill,
  pthread_self,
  pthread_yield,
  semCreate,
  semDestroy,
  semP,
  semV,
  semTryP,
  semTryV,
  semUnlockP,
  privilege,
  _init_sigtrampoline,
  max
};

};

extern "C" ssize_t syscallStub(mword x, mword a1 = 0, mword a2 = 0, mword a3 = 0, mword a4 = 0, mword a5 = 0);

template<class... Args>
static ssize_t syscallWrap(Args... a) {
  FloatingPointFlags fp(true);     // save FP flags, rest of FP saved by ABI
  ssize_t ret = syscallStub(a...);
  fp.restore();                    // restore FP flags
  return ret;
}

#endif /* _syscalls_h_ */
