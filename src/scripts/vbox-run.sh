#!/bin/bash
source ../config
CFGDIR=$(dirname $0)/../../cfg
GDB="$GDBDIR/bin/$TARGET-gdb -b 38400 -x $CFGDIR/gdbinit1"

function leave() {
	echo "arp -d 192.168.56.200"
	echo "arp -d 192.168.56.201"
	echo "iptables -D FORWARD -i vboxnet0 -j ACCEPT"
	echo "iptables -D INPUT -i vboxnet0 -j ACCEPT"
	echo "ifconfig vboxnet0 down"
	vboxmanage unregistervm KOS
}

trap "leave; killall socat; exit 0" SIGHUP SIGINT SIGQUIT SIGTERM

vboxmanage unregistervm KOS 2>/dev/null
rm -f /tmp/KOS.vbox.tmp.*
tmpcfg=$(mktemp /tmp/KOS.vbox.tmp.XXXXXXXX)
if [ "$1" = "gdb" ]; then
	cp $CFGDIR/KOS.vbox.gdb $tmpcfg
else
	cp $CFGDIR/KOS.vbox.default $tmpcfg
fi
vboxmanage registervm $tmpcfg
echo "arp -s 192.168.56.200 0800272BA956"
echo "arp -s 192.168.56.201 0800272BA957"
echo "iptables -I FORWARD -i vboxnet0 -j ACCEPT"
echo "iptables -I INPUT -i vboxnet0 -j ACCEPT"

if [ "$1" = "debug" ]; then
	/usr/lib/virtualbox/VirtualBoxVM --startvm KOS --debug 2> >(grep -F -v libpng)
elif [ "$1" = "gdb" ]; then
	/usr/lib/virtualbox/VirtualBoxVM --startvm KOS --dbg 2> >(grep -F -v libpng) & sleep 5
	socat -b1 TCP-LISTEN:2345,reuseaddr UNIX-CONNECT:/tmp/KOS.pipe & sleep 1
	$GDB -ex "target remote :2345" -x $CFGDIR/gdbinit2 kernel.sys.debug
	kill %1
else
	/usr/lib/virtualbox/VirtualBoxVM --startvm KOS --dbg 2> >(grep -F -v libpng)
fi

leave
exit 0
