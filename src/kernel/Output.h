/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Output_h_
#define _Output_h_ 1

#include "runtime/Bitmap.h"
#include "kernel/KernelLocks.h"

#include <cstdarg>

static const char kendl = '\n';

template<typename _CharT, typename _Traits = std::char_traits<_CharT>>
class OutputBuffer : public std::basic_streambuf<_CharT, _Traits> {
public:
  typedef _CharT													char_type;
  typedef _Traits													traits_type;
  typedef typename traits_type::int_type	int_type;
  typedef typename traits_type::pos_type	pos_type;
  typedef typename traits_type::off_type	off_type;

  typedef std::basic_streambuf<_CharT, _Traits>  BaseClass;

protected:
  virtual std::streamsize xsputn(const char_type* s, std::streamsize n) = 0;
  virtual int sync() { return BaseClass::sync(); }
};

class KernelOutput {
  KernelOwnerLock olock;

public:
  std::ostream os;
  KernelOutput( OutputBuffer<char>& ob ) : os(&ob) {}

  void lock() { olock.acquire(); }
  void unlock() { olock.release(); }

  template <bool cpu> void print() {}

  template<bool cpu, typename T, typename... Args>
  void print( const T& msg, const Args&... a ) {
    if (cpu) os << 'C' << LocalProcessor::getIndex() << '/' << FmtHex(CPU::readCR3()) << ": ";
    os << msg;
    print<false>(a...);
  }

  template<bool cpu, typename T, typename... Args>
  void printl( const T& msg, const Args&... a ) {
    ScopedLock<KernelOwnerLock> sl(olock);
    if (cpu) os << 'C' << LocalProcessor::getIndex() << '/' << FmtHex(CPU::readCR3()) << ": ";
    os << msg;
    print<false>(a...);
  }

  ssize_t write(const char* buf, size_t len) {
    ScopedLock<KernelOwnerLock> sl(olock);
    os.write(buf, len);
    return len;
  }
};

extern KernelOutput StdOut;
extern KernelOutput StdErr;
extern KernelOutput StdDbg;

class KOUT {
public:
  template<typename... Args> static void out1( const Args&... a ) {
    StdOut.printl<false>(a...);
#if TESTING_STDOUT_DEBUG
    StdDbg.printl<false>(a...);
#endif
  }
  template<typename... Args> static void outl( const Args&... a ) {
    StdOut.printl<false>(a..., kendl);
#if TESTING_STDOUT_DEBUG
    StdDbg.printl<false>(a..., kendl);
#endif
  }
};

class KERR {
public:
  template<typename... Args> static void out1( const Args&... a ) {
    StdErr.printl<false>(a...);
#if TESTING_STDOUT_DEBUG
    StdDbg.printl<false>(a...);
#endif
  }
  template<typename... Args> static void outs( const Args&... a ) {
    StdErr.printl<true>(a...);
#if TESTING_STDOUT_DEBUG
    StdDbg.printl<true>(a...);
#endif
  }
  template<typename... Args> static void outl( const Args&... a ) {
    StdErr.printl<true>(a..., kendl);
#if TESTING_STDOUT_DEBUG
    StdDbg.printl<true>(a..., kendl);
#endif
  }
};

#endif /* _Output_h_ */
