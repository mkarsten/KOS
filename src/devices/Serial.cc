/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "machine/Machine.h"
#include "devices/Serial.h"

static const uint16_t SerialIRQs[maxSerial] = { PIC::Serial0, PIC::Serial1 };

void SerialDevice::init(bool g, _friend<Machine>) {
  gdb = g;
  for (int i = 0; i < maxSerial; i += 1) {
    CPU::out8(SerialPort[i] + 1, 0x00);    // Disable all interrupts
    CPU::out8(SerialPort[i] + 3, 0x80);    // Enable DLAB (set baud rate divisor)
    CPU::out8(SerialPort[i] + 0, 0x01);    // Set divisor to 1 (lo byte) 115200 baud
    CPU::out8(SerialPort[i] + 1, 0x00);    //                  (hi byte)
    CPU::out8(SerialPort[i] + 3, 0x03);    // 8 bits, no parity, one stop bit
    CPU::out8(SerialPort[i] + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
    CPU::out8(SerialPort[i] + 4, 0x0B);    // IRQs enabled, RTS/DSR set
  }
}

void SerialDevice::init2(_friend<Machine>) {
  for (int i = 0; i < maxSerial; i += 1) {
    Machine::registerIrqAsync( SerialIRQs[i], irqHandler, nullptr );
  }
}
