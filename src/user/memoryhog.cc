/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "syscalls.h"
#include "pthread.h"

#include <cstdio>
#include <cstring>

static pthread_mutex_t iolock;
static pthread_mutex_t mlock;

static void* task(void* x) {
  while (1) {
    pthread_mutex_lock(&mlock);
    void* p = malloc(1023*1023);
    pthread_mutex_unlock(&mlock);
    pthread_mutex_lock(&iolock);
    printf("%u - %p\n",(uintptr_t)x,p);
    pthread_mutex_unlock(&iolock);
    memset(p, 0, 1023*1023);
  }
}

int main() {
  pthread_mutex_init(&iolock, nullptr);
  pthread_mutex_init(&mlock, nullptr);
  pthread_t t, u;
  pthread_create(&t, nullptr, task, (void*)1);
  pthread_create(&u, nullptr, task, (void*)2);
  task((void*)3);
  return 0;
}
