/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Serial_h_
#define _Serial_h_ 1

#include "machine/CPU.h"

class Machine;

class DebugDevice : public NoObject {
  static bool valid;
public:
  static void init(_friend<Machine>) { valid = (CPU::in8(0xE9) == 0xE9); }
  static inline void write(char c);
};

static const int maxSerial = 2;
static const uint16_t SerialPort[maxSerial] = { 0x3F8, 0x2F8 }; // 0x3E8, 0x2E8
static const int bufmax = 16;
static int bufcount[maxSerial] = { bufmax, bufmax };

class SerialDevice : public NoObject { // see http://wiki.osdev.org/Serial_Ports
  static bool gdb;
  static void irqHandler(ptr_t) {}

public:
  static void init(bool g, _friend<Machine>);
  static void init2(_friend<Machine>);

  static void write(char c, uint16_t idx = 0) {
    if (bufcount[idx] == 0) {
      while ((CPU::in8(SerialPort[idx] + 5) & 0x20) == 0) Pause();
      bufcount[idx] = bufmax;
    }
    CPU::out8(SerialPort[idx], c);
    bufcount[idx] -= 1;
  }

  static char read(uint16_t idx = 0) {
    while ((CPU::in8(SerialPort[idx] + 5) & 0x01) == 0) Pause();
    return CPU::in8(SerialPort[idx]);
  }

  static void dbgwrite(char c, _friend<DebugDevice>) {
    for (uint16_t idx = gdb ? 1 : 0; idx < maxSerial; idx += 1) {
      write(c, idx);
      if (c == '\n') write('\r', idx);
    }
  }
};

void DebugDevice::write(char c) {
  if (valid) CPU::out8(0xE9, c);
  SerialDevice::dbgwrite(c, _friend<DebugDevice>());
}

#endif /* _Serial_h_ */
