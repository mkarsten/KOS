/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "ulib/pthread.h"

#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;

static const int tcount = 16;

static pthread_mutex_t iolock;

static int signum = 0;

static void sighandler(int s) {
  signum = s;
}

static void* task(void*) {
  int cid = getcid();
  double b = 4.124;
  double x = b * (cid+1);
  double s = 0;
  for (int i = 0; i < 1000000; i += 1) s = s + x;
  uint64_t s1 = round(x * 1000000.0);
  uint64_t s2 = round(s);
  int cid2 = getcid();
  pthread_mutex_lock(&iolock);
  cout << "FP result: " << ' ' << s1 << ' ' << s2 << ' ' << cid << ' ' << cid2;
  if (s1 == 0 || s1 != s2) cout << "  FP mismatch!";
  cout << endl;
  pthread_mutex_unlock(&iolock);
  return nullptr;
}

int main() {
  pthread_mutex_init(&iolock, nullptr);
  pthread_t th[tcount];
  for (int i = 0; i < tcount; i += 1) {
    pthread_create(&th[i], nullptr, task, nullptr);
  }
  _user_sighandler = sighandler;
  void* result;
  for (int i = 0; i < tcount; i += 1) {
    pthread_join(th[i], &result);
  }
  printf("FP: signal 0x%x\n", signum);
  return 0;
};
