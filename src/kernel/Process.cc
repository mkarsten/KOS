/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "kernel/Process.h"
#include "extern/elfio/elfio.hpp"

Process::PCBStore* Process::pcbStore = nullptr;
KernelLock         Process::pcbLock;

void Process::invokeUser(funcvoid2_t func, ptr_t arg1, ptr_t arg2) {
  UserThread* ut = Process::CurrUT();
  ut->ustackSize = defaultUStackSize;
  ut->ustackBottom = CurrProcess().allocStack(ut->ustackSize);
  DBG::outl(DBG::Threads, "UserThread start: ", FmtHex(ut), '/', FmtHex((ptr_t)func), " ustack:", FmtHex(ut->ustackBottom), '/', FmtHex(ut->ustackSize));
  start_user_code(arg1, arg2, vaddr(ut), func, ut->ustackBottom + ut->ustackSize);
  RABORT0();
}

void Process::loadAndRun(Process* p, char* fileName, uintptr_t len) {
  funcvoid2_t entry = p->load(fileName, len);
  DBG::outl(DBG::ProcessDbg, "Process entry: ", FmtHex(p), ' ', FmtHex(ptr_t(entry)));
  invokeUser(entry, nullptr, nullptr);
}

static KernelLock elfLock;

inline funcvoid2_t Process::load(char* fileName, uintptr_t len) {
  RASSERT0(tcbStore.size() == 1);
  auto iter = kernelFS.find(fileName);
  RASSERT(iter != kernelFS.end(), fileName)
  RamFile& rf = iter->second;
  ScopedLock<KernelLock> sl(elfLock);
  ELFIO::elfio elfReader;
  if (!elfReader.load(fileName)) RABORT(fileName);
  RASSERT(elfReader.get_class() == ELFCLASS64, elfReader.get_class());

  DBG::outl(DBG::ProcessDbg, "Process load: ", FmtHex(this), ' ', fileName);

  kdelete(fileName, len);

  vaddr currBreak = 0;
  for (int i = 0; i < elfReader.segments.size(); ++i) {
    const ELFIO::segment* pseg = elfReader.segments[i];
    if (pseg->get_type() != PT_LOAD) continue;  // not a loadable segment

    RASSERT(pseg->get_file_offset() + pseg->get_file_size() <= rf.size, FmtHex(pseg->get_file_offset()), ' ', FmtHex(pseg->get_file_size()), ' ', FmtHex(rf.size));
    RASSERT(pseg->get_memory_size() >= pseg->get_file_size(), FmtHex(pseg->get_file_size()), ' ', FmtHex(pseg->get_memory_size()));
    paddr pma = rf.pma + pseg->get_file_offset();
    paddr apma = align_down(pma, smallps);
    vaddr vma = pseg->get_virtual_address();
    vaddr avma = align_down(vma, smallps);
    RASSERT(vma - avma == pma - apma, FmtHex(vma), ' ', FmtHex(pma));
    vaddr fend = vma + pseg->get_file_size();
    vaddr afend = align_up(fend, smallps);
    vaddr mend = vma + pseg->get_memory_size();
    vaddr amend = align_up(mend, smallps);

    // If .rodata and .text are in the same elf segment and small enough to
    // fit into a single page, then .rodata ends up being marked executable.
    PageType pageType = (pseg->get_flags() & PF_W) ? Data :
      (pseg->get_flags() & PF_X) ? Code : RoData;

    DBG::outl(DBG::ProcessDbg, "Process ",
      pageType == Data ? "data" : pageType == Code ? "code" : "ro  ",
      " segment: ", FmtHex(vma), '-', FmtHex(fend));
    mapDirect<smallpl>(apma, avma, afend - avma, pageType);

    if (mend > fend) {
      DBG::outl(DBG::ProcessDbg, "Process bss  segment: ", FmtHex(fend), '-', FmtHex(mend));
      if (amend > afend) { // newly mapped memory is already wiped
        allocDirect<smallpl>(afend, amend - afend, Data);
        memset((ptr_t)fend, 0, afend - fend);
      } else {             // leftover from binary: wipe to play it safe...
        memset((ptr_t)fend, 0, mend - fend);
      }
    }
    if (mend > currBreak) currBreak = mend;
  }

  setup(currBreak);
  state = Running;
  return (funcvoid2_t)elfReader.get_entry();
}

Process::~Process() {
  DBG::outl(DBG::ProcessDbg, "Process delete: ", FmtHex(this));
  for (size_t i = 0; i < ioHandles.currentIndex(); i += 1) {
    Access* a = ioHandles.removeForce(i);
    if (a) kdelete(a);
  }
  ScopedLock<KernelLock> sl(semLock);
  for (size_t i = 0; i < semStore.currentIndex(); i += 1) {
    if (semStore.valid(i)) kdelete(semStore.access(i));
  }
}

mword Process::exec(const std::string& fn) {
  pcbLock.acquire();
  index = pcbStore->put(this);
  pcbLock.release();
  uintptr_t len = fn.size();
  char* fileName = knewN<char>(len);
  strcpy(fileName, fn.c_str());
  DBG::outl(DBG::ProcessDbg, "Process exec: ", FmtHex(this), ' ', fn, ' ', index);
  createThreadInternal((ptr_t)Process::loadAndRun, this, fileName, (ptr_t)len);
  return index;
}

inline mword Process::createThreadInternal(ptr_t invoke, ptr_t wrapper, ptr_t func, ptr_t data) {
  vaddr mem = kernelAS.allocStack(defaultKStackSize);
  ScopedLock<KernelLock> sl(tcbLock);
  if (state == Exiting) {
    kernelAS.releaseStack(mem, defaultKStackSize);
    return limit<mword>();
  }
  UserThread* ut = UserThread::create(mem, defaultKStackSize, *this);
  threadCount += 1;
  ut->index = tcbStore.put(ut);
  DBG::outl(DBG::Threads, "UserThread TCB add: ", FmtHex(ut), '/', ut->index);
  ut->start(invoke, wrapper, func, data);
  return ut->index;
}

mword Process::startThread(funcvoid2_t wrapper, funcvoid1_t func, ptr_t data) {
  return createThreadInternal((ptr_t)invokeUser, (ptr_t)wrapper, (ptr_t)func, data);
}

void Process::detachThread(mword tid) {
  DBG::outl(DBG::Threads, "UserThread detach: ", tid);
  ScopedLock<KernelLock> sl(tcbLock);
  if (!tcbStore.valid(tid)) return;
  tcbStore.access(tid).detach();
}

void Process::exitThread(ptr_t result) {
  UserThread* ut = CurrUT();
  DBG::outl(DBG::Threads, "UserThread exit: ", FmtHex(ut), '/', ut->index);
  releaseStack(ut->ustackBottom, ut->ustackSize); // release user-level stack
  tcbLock.acquire();
  RASSERT(tcbStore.valid(ut->index), ut->index);
  TCB& tcb = tcbStore.access(ut->index);
  if (!tcb.post(result)) { // thread is detached
    tcbStore.remove(ut->index);
    if (tcbStore.empty()) AddressSpace::clean();
  }
  tcbLock.release();
  ut->terminate();
}

int Process::joinThread(mword tid, ptr_t& result) {
  DBG::outl(DBG::Threads, "UserThread join: ", tid);
  ScopedLock<KernelLock> sl(tcbLock);
  if (!tcbStore.valid(tid)) return -ESRCH;
  TCB& tcb = tcbStore.access(tid);
  if (tcb.wait(tcbLock, result)) {
    tcbStore.remove(tid);
    if (tcbStore.empty()) AddressSpace::clean();
    return 0;
  } else {
    return -EINVAL;
  }
}

int Process::signalThread(mword tid, mword sig) {
  DBG::outl(DBG::Threads, "UserThread signal: ", tid, ' ', sig);
  ScopedLock<KernelLock> sl(tcbLock);
  if (!tcbStore.valid(tid)) return -ESRCH;
  TCB& tcb = tcbStore.access(tid);
  if (tcb.posted()) return -ESRCH;
  tcb.getRunner()->sigPending |= sig;
  return 0;
}

void Process::detach() {
  ScopedLock<KernelLock> sl(pcbLock);
  RASSERT(pcbStore->valid(index), index);
  pcbStore->access(index).detach();
}

void Process::exit(int result) {
  DBG::outl(DBG::Threads, "Process exit: ", FmtHex(this), ' ', FmtHex(result));
  tcbLock.acquire();
  RASSERT0(state != Exiting);
  state = Exiting;
  for (size_t i = 0; i < tcbStore.currentIndex(); i += 1) {
    if (tcbStore.valid(i)) {
      tcbStore.access(i).getRunner()->sigPending |= SIGTERM;
      tcbStore.access(i).detach();
    }
  }
  tcbLock.release();
  ScopedLock<KernelLock> sl(pcbLock);
  RASSERT(pcbStore->valid(index), index);
  if (!pcbStore->access(index).post(result)) { // process is detached
    pcbStore->remove(index);
  }
}

int Process::join(mword pid, int& result) {
  DBG::outl(DBG::Threads, "Process join: ", pid);
  ScopedLock<KernelLock> sl(pcbLock);
  if (!pcbStore->valid(pid)) return -ESRCH;
  if (pcbStore->access(pid).wait(pcbLock, result)) {
    pcbStore->remove(pid);
    return 0;
  } else {
    return -EINVAL;
  }
}

int Process::kill(mword pid, int sig) {
  DBG::outl(DBG::Threads, "Process kill: ", pid, ' ', sig);
  ScopedLock<KernelLock> sl(pcbLock);
  if (!pcbStore->valid(pid)) return -ESRCH;
  if (pcbStore->access(pid).posted()) return -ESRCH;
  Process* p = pcbStore->access(pid).getRunner();
  if (sig == SIGTERM) p->exit(-1);
  else p->setSignal(sig);
  return 0;
}

void Process::preThreadSwitch(Thread& currThread) {
  UserThread& ut = reinterpret_cast<UserThread&>(currThread);
  // save user-level context (fs register)
  ut.uctx.save();
  // FP save - conditional on ISR and TS=0
  if (ut.isrMode) {
    if (!CPU::tstTS()) ut.fpctx.save();
  } else {
    ut.fpctx.setClean();
  }
}

void Process::postThreadSwitch(Thread& newThread) {
  UserThread& ut = reinterpret_cast<UserThread&>(newThread);
  // restore user-level context (fs register)
  ut.uctx.restore();
  // lazy FP restore? - see https://lkml.org/lkml/2011/7/24/122
  if (!ut.fpctx.isClean()) CPU::setTS();
}

// after destruction  of the last thread of a process -> destroy process
bool Process::threadTerminated() {
  ScopedLock<KernelLock> sl(tcbLock);
  threadCount -= 1;
  return threadCount == 0;
}
