/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RTC_h_
#define _RTC_h_ 1

#include "machine/CPU.h"
#include <ctime>

class RTC : public NoObject {
  static inline uint8_t read(uint8_t reg) {
    CPU::out8(0x70,reg);
    return CPU::in8(0x71);
  }
  static inline void write(uint8_t reg, uint8_t val) {
    CPU::out8(0x70,reg);
    CPU::out8(0x71,val);
  }
public:
  static void init(mword hz, mword& eN, mword& eD)      __section(".boot.text");
  static time_t readCurrentTime();
  static void staticInterruptHandler() {
    read(0x0C);           // read Register C -> needed to keep interrupts coming
  }
};

#endif /* _RTC_h_ */
