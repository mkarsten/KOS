/******************************************************************************
    Copyright (C) Martin Karsten 2015-2021

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _RuntimeFred_h_
#define _RuntimeFred_h_ 1

#include "kernel/Process.h"

inline void RuntimeStartFred(funcvoid3_t func, ptr_t arg1, ptr_t arg2, ptr_t arg3) {
  func(arg1, arg2, arg3);
}

inline void RuntimePreFredSwitch(Fred& currFred, Fred& nextFred, _friend<Fred> fs) {
  Thread& currThread = reinterpret_cast<Thread&>(currFred);
  Thread& nextThread = reinterpret_cast<Thread&>(nextFred);
  AddressSpace& currAS = currThread.getAS();
  currAS.preThreadSwitch(currThread);
  currAS.switchTo(nextThread.getAS());
  LocalProcessor::setCurrFred(&nextFred, fs);
}

inline void RuntimePostFredSwitch(Fred& newFred, _friend<Fred> fs) {
  Thread& newThread = reinterpret_cast<Thread&>(newFred);
  newThread.getAS().postThreadSwitch(newThread);
}

inline void RuntimeFredDestroy(Fred& prevFred, _friend<Fred> fs) {
  Thread& prevThread = reinterpret_cast<Thread&>(prevFred);
  if (prevThread.getAS().threadTerminated()) {
    Process* p = &reinterpret_cast<Process&>(prevThread.getAS());
    delete p;
  }
  prevThread.destroy(fs);
}

#endif /* _RuntimeFred_h_ */
